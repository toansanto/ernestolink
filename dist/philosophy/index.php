<?php
session_start();
header("Cache-control: private");
header_remove("Expires");
header_remove("Cache-Control");
header_remove("Pragma");
header_remove("Last-Modified");
ob_start();
if (!isset($_SESSION['unique'])) {
	$_SESSION['unique'] = time();
}
include_once(dirname(__DIR__) . '/app_config.php');
$thisPageName = 'philosophy';
include(APP_PATH.'libs/head.php');
?>
<meta http-equiv="expires" content="86400">
<meta name="format-detection" content="telephone=no">
<link
    rel="stylesheet"
    href="<?php echo APP_ASSETS ?>css/page/philosophy.min.css">
</head>

<body id="philosophy" class="philosophy">
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
    <main>
        <div class="cmn_hd">
            <div class="cmn_hd__inner">
                <div class="cmn_hd__bg">
                    <span
                        class="thumb lazy pc"
                        data-bg="<?php echo APP_ASSETS;?>img/philosophy/img_main.jpg"></span>
                    <span
                        class="thumb lazy sp"
                        data-bg="<?php echo APP_ASSETS;?>img/philosophy/img_main_sp.jpg"></span>
                </div>
                <div class="wcm cmn_hd__title">
                    <span class="cmn_hd__title--en">PHILOSOPHY</span>
                    <h1 class="cmn_hd__title--jp">企業理念</h1>
                </div>
            </div>
        </div>
        <div class="breadcrumbs">
            <ul class="wcm">
                <li>
                    <a href="<?php echo APP_URL;?>">TOP</a>
                </li>
                <li>
                    <span>企業理念</span></li>
            </ul>
        </div>
        <div class="philosophy__content">
            <div class="content01">
                <div class="content01__title inner">
                    <span class="content01__title__ico"><img src="<?php echo APP_ASSETS;?>img/philosophy/icon.png" alt="icon"></span>
                    <h4>私たちは安定経営の仕組みづくりを<br class="sp">お手伝い致します</h4>
                    <p>現在、日本には４００万社以上の企業があり、そのうち９９％以上が中小企業です。<br class="pc">
                        日本経済を支える中小企業が元気になれば、日本も必ず元気になります。<br>
                        弊社エルネストリンクは「中小企業の右腕」として様々な経営課題を解決するサービスを展開し、<br class="pc">
                        貴社の安定経営をサポートします。</p>
                </div>
                <div class="content01__text">
                    <h5>〈過去の実績や<br class="sp">ビッグデータを活用したコスト削減と<br class="sp">集客を使った売上げUPが同時に実現〉</h5>
                    <div class="content01__text__content">
                        <div class="inner pc">
                            <img
                                src="<?php echo APP_ASSETS;?>img/philosophy/text.svg"
                                alt="過去の実績やビッグデータを活用したコスト削減と集客を使った売上げUPが同時に実現"></div>
                        <div class="sp">
                            <img
                                src="<?php echo APP_ASSETS;?>img/philosophy/text-sp01.svg"
                                alt="過去の実績やビッグデータを活用したコスト削減と集客を使った売上げUPが同時に実現">
                            <img
                                src="<?php echo APP_ASSETS;?>img/philosophy/text-sp02.svg"
                                alt="過去の実績やビッグデータを活用したコスト削減と集客を使った売上げUPが同時に実現"></div>
                    </div>
                </div>
            </div>
            <div class="content02">
                <p class="content02__wwd inner"><img
                    src="<?php echo APP_ASSETS;?>img/philosophy/whatwedo.png"
                    alt="whatwedo"
                    class="pc"><img
                    src="<?php echo APP_ASSETS;?>img/philosophy/whatwedo-sp.png"
                    alt="whatwedo"
                    class="sp"></p>
                <div class="content02__listtodo">
                    <ul class="content02__listtodo__box inner">
                        <li class="content02__listtodo__box__info">
                            <span class="title">
                                <img
                                    src="<?php echo APP_ASSETS;?>img/philosophy/things.png"
                                    alt="私たちにできること"
                                    class="pc">
                                <img
                                    src="<?php echo APP_ASSETS;?>img/philosophy/things-sp.png"
                                    alt="私たちにできること"
                                    class="sp"></span>
                            <div class="contentinfo">
                                <em><img src="<?php echo APP_ASSETS;?>img/philosophy/01.svg" alt="01"></em>
                                <div class="left"><img src="<?php echo APP_ASSETS;?>img/philosophy/img01.jpg" alt="安定経営を実現"></div>
                                <div class="right">
                                    <h5>安定経営を実現</h5>
                                    <p>企業の成長による売上高の増加に伴い、固定費も膨れ上がります。<br>
                                        ただ、その固定費が利益を圧迫しているのも事実です。そして、固定費の利益圧迫は様々なコスト構造を見直すことで改善可能です。私たちはノーコスト、完全成功報酬型のコスト削減で、御社の安定経営の実現をお手伝いします。</p>
                                </div>
                            </div>
                        </li>
                        <li class="content02__listtodo__box__info">
                            <span class="title">
                                <img
                                    src="<?php echo APP_ASSETS;?>img/philosophy/things.png"
                                    alt="私たちにできること"
                                    class="pc">
                                <img
                                    src="<?php echo APP_ASSETS;?>img/philosophy/things-sp.png"
                                    alt="私たちにできること"
                                    class="sp"></span>
                            <div class="contentinfo">
                                <em><img src="<?php echo APP_ASSETS;?>img/philosophy/02.svg" alt="02"></em>
                                <div class="left"><img src="<?php echo APP_ASSETS;?>img/philosophy/img02.jpg" alt="キャッシュフローの改善"></div>
                                <div class="right">
                                    <h5>キャッシュフローの改善</h5>
                                    <p>借り入れなどによる返済金は税引後利益から返済しなければなりません。例えば、１０００万円の返済があるとすれば、１０００万円＋法人税＋金利のキャッシュが必要になります。コスト削減はキャッシュを生み出し、税引前利益に直結するので、キャッシュフロー改善に役立ちます。
                                    </p>
                                </div>
                            </div>
                        </li>
                        <li class="content02__listtodo__box__info">
                            <span class="title">
                                <img
                                    src="<?php echo APP_ASSETS;?>img/philosophy/things.png"
                                    alt="私たちにできること"
                                    class="pc">
                                <img
                                    src="<?php echo APP_ASSETS;?>img/philosophy/things-sp.png"
                                    alt="私たちにできること"
                                    class="sp"></span>
                            <div class="contentinfo">
                                <em><img src="<?php echo APP_ASSETS;?>img/philosophy/03.svg" alt="03"></em>
                                <div class="left"><img
                                    src="<?php echo APP_ASSETS;?>img/philosophy/img03.jpg"
                                    alt="導入後の総務、経理担当者様の負担を削減"></div>
                                <div class="right">
                                    <h5>導入後の総務、<br>
                                        経理担当者様の負担を削減</h5>
                                    <p>
                                        導入後は総務、経理担当者の方と弊社専用のグループウェアなどで、各項目の契約期限、見直し時期などを告知します。導入後の管理工数を減らし、弊社にてサポートします。総務、経理担当者の方は、本来すべき業務に集中していただけます。</p>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="content03">
                <div class="content03__performance--title">
                    <p class="title_en">
                        <span>P</span>ERFORMANCE</p>
                    <h2 class="title_jp">これまでの活動実績</h2>
                </div>
                <ul class="content03__box inner">
                    <li class="content03__box__info">
                        <div class="content03__box__info__img img1">
                            <p class="up">削減成功率</p>
                            <p class="down">
                                <em>92</em>
                                <span>%</span></p>
                        </div>
                        <h5>〈人件費以外は下げられます〉</h5>
                        <p>どんな企業でも必ず削減できるところがあります。時代の変化と共に生まれる「経費のムダ」を最新のビッグデータの活用で削減。過去1000社以上の企業様の実績で削減成功率は92%。</p>
                    </li>
                    <li class="content03__box__info">
                        <div class="content03__box__info__img img2">
                            <p class="up">導入件数</p>
                            <p class="down">
                                <em>1200</em>
                                <span>件以上</span></p>
                        </div>
                        <h5>〈モノを良くした上で安くする〉</h5>
                        <p>導入企業様や関連企業様からのご紹介だけで全国から依頼をいただいております。<br class="sp">「モノを良くした上で安くする」の考え方で、安全に企業のコストを削減し、安定経営のお手伝いをさせていただいております。</p>
                    </li>
                    <li class="content03__box__info">
                        <div class="content03__box__info__img img3">
                            <p class="up">継続削減率</p>
                            <p class="down">
                                <em>81</em>
                                <span>%</span></p>
                        </div>
                        <h5>〈削減して終わりではありません〉</h5>
                        <p>コスト削減後、グループウェアを組ませていただき、専任の弊社サポート担当者がアフターフォローをさせていただきます。<br class="sp">これにより、コスト削減した項目の管理コストも継続して削減できます。</p>
                    </li>
                    <li class="content03__box__info">
                        <div class="content03__box__info__img img4">
                            <p class="up">パートナー企業</p>
                            <p class="down">
                                <em>57</em>
                                <span>社</span></p>
                        </div>
                        <h5>〈新規営業はしていません〉</h5>
                        <p>大手税理士法人、社労事務所、金融機関などの有資格者の方々と提携して事業を行っております。キャッシュフローの専門家からの信頼も高く、数多くのご紹介をいただいております。</p>
                    </li>
                </ul>
            </div>
        </div>
    </main>
</div>
<?php
include(APP_PATH.'libs/service.php');
include(APP_PATH.'libs/footer.php');
?>
<script src="<?php echo APP_ASSETS; ?>js/form/ajaxzip3.js"></script>
<script src="<?php echo APP_ASSETS; ?>js/form/jquery.validationEngine.js"></script>
<script
    src="<?php echo APP_ASSETS; ?>js/form/languages/jquery.validationEngine-ja.js"></script>
<script src="<?php echo APP_ASSETS; ?>js/page/contact.min.js"></script>
</body>

</html>