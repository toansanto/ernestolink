<?php
include(APP_PATH.'libs/head.php');
?>
<meta name="robots" content="noindex,nofollow">
<meta http-equiv="refresh" content="15; url=<?php echo APP_URL ?>">
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/404.min.css">
</head>
<body id="page404" class="page404">
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
  <main>
    <div class="cmn_hd">
      <div class="cmn_hd__inner">
        <div class="cmn_hd__bg">
          <span class="thumb lazy pc" data-bg="<?php echo APP_ASSETS;?>img/contact/img_main.jpg"></span>
          <span class="thumb lazy sp" data-bg="<?php echo APP_ASSETS;?>img/contact/img_main_sp.jpg"></span>
        </div>
        <div class="wcm cmn_hd__title">
          <span class="cmn_hd__title--en">404</span>
          <h1 class="cmn_hd__title--jp">Page Not Found</h1>
        </div>
      </div>
    </div>
    <div class="breadcrumbs">
      <ul class="wcm">
        <li><a href="<?php echo APP_URL;?>">TOP</a></li>
        <li><span>404 - Page Not Found</span></li>
      </ul>
    </div>
    <div class="wcm page404__inner">
      <div class="page404__content">
        <p class="page404__content--desc">アクセスしようとしたページは、<br class="sp">移動したか削除されました。<br>下記リンクに移動して下さい。</p>
        <a class="cmn_btn btn_border" href="<?php echo APP_URL;?>"><span>トップページに戻る</span></a>
      </div>
    </div>
  </main>
</div>
<?php include(APP_PATH.'libs/footer.php'); ?>
<script>
	var href="<?php echo APP_URL."404.php";?>";
	window.history.pushState('',href,href);
</script>
</body>
</html>
