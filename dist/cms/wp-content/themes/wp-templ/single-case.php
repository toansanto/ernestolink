<?php
$thisPageName = 'case_detail';
$title_ori = $post->post_title;
$fields = get_fields();
$mainimg = get_post_thumb_meta($post->ID);
if (!empty($mainimg)) $ogimg = $mainimg['file'];
include(APP_PATH.'libs/head.php');
?>
<link rel="stylesheet" href="<?php echo APP_ASSETS;?>css/page/case.min.css">
</head>
<body id="case" class="case case_detail">
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
  <main>
    <div class="cmn_hd">
      <div class="cmn_hd__inner">
        <div class="cmn_hd__bg">
          <span class="thumb lazy pc" data-bg="<?php echo APP_ASSETS;?>img/case/img_main.jpg"></span>
          <span class="thumb lazy sp" data-bg="<?php echo APP_ASSETS;?>img/case/img_main_sp.jpg"></span>
        </div>
        <div class="wcm cmn_hd__title">
          <span class="cmn_hd__title--en">Case study</span>
          <h1 class="cmn_hd__title--jp">導入事例</h1>
        </div>
      </div>
    </div>
    <div class="breadcrumbs">
      <ul class="wcm">
        <li><a href="<?php echo APP_URL;?>">TOP</a></li>
        <li><a href="<?php echo APP_URL;?>case/">導入事例</a></li>
        <li><span><?php echo $title_ori;?>様</span></li>
      </ul>
    </div>
    <div class="wcm2 case__inner">
      <div class="case_detail__intro">老舗焼肉店が年間2,000万円の<br class="sp">コスト削減に成功。<br>コロナでも黒字回復できる経営体質に。</div>
      <div class="case__content">
        <div class="case__content--left">
          <?php if ($ogimg != '') { ?>
          <img class="case_detail__img lazy" src="<?php echo createSVG($mainimg['width'],$mainimg['height']);?>" data-src="<?php echo $mainimg['file'];?>" alt="<?php echo $title_ori;?>">
          <?php } ?>
          <h1 class="case_detail__title"><?php echo $title_ori;?></h1>
          <?php if ($fields['industry'] != '' || $fields['business'] != '' || $fields['url'] != '') { ?>
          <ul class="case_detail__points">
            <?php if ($fields['industry'] != '') { ?>
            <li>
              <span class="lb01">業　種</span>
              <p><?php echo $fields['industry'];?></p>
            </li>
            <?php } ?>
            <?php if ($fields['business'] != '') { ?>
            <li>
              <span class="lb02">事業内容</span>
              <p><?php echo $fields['business'];?></p>
            </li>
            <?php if ($fields['url'] != '') { ?>
            <?php } ?>
            <li>
              <span class="lb03">ホームページ</span>
              <p><a href="<?php echo $fields['url'];?>"><?php echo $fields['url'];?></a></p>
            </li>
            <?php } ?>
          </ul>
          <?php } ?>
          <?php if ($fields['content'] != '') { ?>
          <div class="cmsContent case_detail__desc">
            <?php echo $fields['content'];?>
          </div>
          <?php } ?>
          <?php if ($fields['customer_title'] != '' || $fields['customer_company'] != '' || $fields['customer_content'] != '') { ?>
          <div class="case_detail__customer">
            <h2 class="case_detail__customer--title">お客さまのコストのお悩み</h2>
            <?php if ($fields['customer_title'] != '') { ?>
            <h3 class="case_detail__customer--subtitle"><?php echo $fields['customer_title'];?></h3>
            <?php } ?>
            <?php if ($fields['customer_company'] != '') { ?>
            <p class="case_detail__customer--per"><?php echo $fields['customer_company'];?></p>
            <?php } ?>
            <?php if ($fields['customer_content'] != '') { ?>
            <div class="case_detail__customer--ct">
              <div class="cmsContent txt">
                <?php echo $fields['customer_content'];?>
              </div>
            </div>
            <?php } ?>
          </div>
          <?php } ?>
          <?php if ($fields['ernes_title01'] != '' || $fields['ernes_content01'] != '') { ?>
          <div class="case_detail__part p_green">
            <h2 class="case_detail__part--title">エルネストリンクからのご提案</h2>
            <div class="case_detail__part--content">
              <?php if ($fields['ernes_title01'] != '') { ?>
              <h3 class="title"><?php echo $fields['ernes_title01'];?></h3>
              <?php } ?>
              <?php if ($fields['ernes_content01'] != '') { ?>
              <p><?php echo $fields['ernes_content01'];?></p>
              <?php } ?>
            </div>
          </div>
          <?php } ?>
          <?php if ($fields['ernes_title02'] != '' || $fields['ernes_content02'] != '') { ?>
          <div class="case_detail__part p_red">
            <h2 class="case_detail__part--title">エルネストリンクからのご提案</h2>
            <div class="case_detail__part--content">
              <?php if ($fields['ernes_title02'] != '') { ?>
              <h3 class="title"><?php echo $fields['ernes_title02'];?></h3>
              <?php } ?>
              <?php if ($fields['ernes_content02'] != '') { ?>
              <p><?php echo $fields['ernes_content02'];?></p>
              <?php } ?>
            </div>
          </div>
          <?php } ?>
        </div>
        <?php include_once(APP_PATH.'libs/sidebar_case.php');?>
      </div>
    </div>
  </main>
</div>
<?php include(APP_PATH.'libs/footer.php'); ?>
<script src="<?php echo APP_ASSETS;?>js/page/case.min.js"></script>
</body>
</html>
