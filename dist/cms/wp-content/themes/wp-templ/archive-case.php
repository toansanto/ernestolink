<?php
$thisPageName = 'case';
$posts_per_page = 6;
include(APP_PATH.'libs/head.php');
?>
<link rel="stylesheet" href="<?php echo APP_ASSETS;?>css/page/case.min.css">
<script>var posts_per_page = <?php echo $posts_per_page;?>;var taxonomy = "<?php echo ($tax_term != '') ? $tax_term->term_id : '';?>";</script>
</head>
<body id="case" class="case case_archive">
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
  <main>
    <div class="cmn_hd">
      <div class="cmn_hd__inner">
        <div class="cmn_hd__bg">
          <span class="thumb lazy pc" data-bg="<?php echo APP_ASSETS;?>img/case/img_main.jpg"></span>
          <span class="thumb lazy sp" data-bg="<?php echo APP_ASSETS;?>img/case/img_main_sp.jpg"></span>
        </div>
        <div class="wcm cmn_hd__title">
          <span class="cmn_hd__title--en">Case study</span>
          <h1 class="cmn_hd__title--jp">導入事例</h1>
        </div>
      </div>
    </div>
    <div class="breadcrumbs">
      <ul class="wcm">
        <li><a href="<?php echo APP_URL;?>">TOP</a></li>
        <li><span>導入事例</span></li>
      </ul>
    </div>
    <div class="wcm2 case__inner">
      <div class="case_archive__intro">
        <h2 class="case_archive__intro--title">実際に導入した企業さまの事例を<br class="sp">ご紹介いたします</h2>
        <p class="case_archive__intro--desc">コスト削減サービスを実際にご利用いただいた企業さま、お客さまに<br class="pc">導入の決め手や導入後の効果について詳しくお聞きしました。<br>エルネストリンクのコスト削減サービスの導入をご検討されている方もぜひ参考にご覧ください。</p>
      </div>
      <?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
      query_posts($query_string . '&post_status=publish&posts_per_page='.$posts_per_page.'&orderby=date&order=desc&paged=' . $paged.$querylogin);
      if (have_posts()) : ?>
      <div class="case__content">
        <div class="case__content--left">
          <ul class="case_archive__list">
          <?php
          while (have_posts()) : the_post();
          $terms = wp_get_post_terms($post->ID,'casecat',array('fields'=>'names'));
          $thumb = (get_the_post_thumbnail_url()) ? get_the_post_thumbnail_url() : APP_ASSETS.'img/common/other/img_nophoto.jpg';
          $chk_new = checkNew($post->ID);
          ?>
            <li class="case_archive__list--item">
              <a href="<?php echo the_permalink();?>">
                <div class="content">
                  <?php if ($chk_new == true) { ?><span class="new">New</span><?php } ?>
                  <?php if ($terms) { ?><span class="cat"><?php echo $terms[0];?></span><?php } ?>
                  <div class="img">
                    <span class="thumb lazy" data-bg="<?php echo $thumb;?>"></span>
                  </div>
                  <div class="txt">
                    <h3 class="title"><?php echo $post->post_title;?>様</h3>
                    <p class="desc"><?php echo makeDesc($post->post_content);?></p>
                  </div>
                </div>
              </a>
            </li>
          <?php endwhile; ?>
          </ul>
          <?php if ($wp_query->max_num_pages > 1) { ?>
          <a class="btn_loadmore" href="javascript:;" data-page="2">さらに表示する</a>
          <?php } ?>
        </div>
        <?php include_once(APP_PATH.'libs/sidebar_case.php');?>
      </div>
      <?php endif; ?>
    </div>
  </main>
</div>
<?php include(APP_PATH.'libs/footer.php'); ?>
<script src="<?php echo APP_ASSETS;?>js/page/case.min.js"></script>
</body>
</html>
