<?php
function my_custom_case_post_type() {
  register_post_type('case', array (
    'labels'                  => array (
      'name'                  => __( '導入事例' ),
      'singular_name'         => __( '導入事例' ),
      'add_new'               => __( '新しく導入事例を書く' ),
      'add_new_item'          => __( '導入事例記事を書く' ),
      'edit_item'             => __( '導入事例記事を編集' ),
      'new_item'              => __( '新しい導入事例記事' ),
      'view_item'             => __( '導入事例記事を見る' ),
      'search_staff'          => __( '導入事例記事を探す' ),
      'not_found'             => __( '導入事例記事はありません' ),
      'not_found_in_trash'    => __( 'ゴミ箱に導入事例記事はありません' ),
      'parent_item_colon'     => ''
    ),
    'public'                  => true,
    'rewrite'                 => true,
    'show_ui'                 => true,
    'supports'                => array ( 'title', 'revisions', 'thumbnail' ),
    'query_var'               => true,
    'menu_icon'               => 'dashicons-welcome-write-blog',
    // 'taxonomies'              => array ( 'post_tag' ),
    'has_archive'             => true,
    'hierarchical'            => false,
    'menu_position'           => 5,
    'capability_type'         => 'post',
    'show_in_admin_bar'       => true,
    'publicly_queryable'      => true,
  ));
}
add_action ( 'init', 'my_custom_case_post_type' );

function create_cat_taxonomy_casecat () {
  register_taxonomy('casecat', 'case', array (
    'labels'                  => array (
      'name'                  => __( 'カテゴリー' ),
      'menu_name'             => __( 'カテゴリー' ),
      'edit_item'             => __( 'カテゴリ記事を編集' ),
      'all_items'             => __( 'カテゴリー' ),
      'parent_item'           => __( '親カテゴリー' ),
      'add_new_item'          => __( 'カテゴリ記事を書く' ),
      'search_items'          => __( 'カテゴリー' ),
      'singular_name'         => __( 'カテゴリー' ),
      'parent_item_colon'     => __( '親カテゴリー:' ),
    ),
    'show_ui'                 => true,
    'rewrite'                 => array ( 'slug' => 'casecat' ),
    'query_var'               => true,
    'has_archive'             => true,
    'hierarchical'            => true,
    'show_admin_column'       => true,
  ));
}
add_action ( 'init', 'create_cat_taxonomy_casecat', '0' );
