<?php
function my_custom_news_post_type() {
  register_post_type('news', array (
    'labels'                  => array (
      'name'                  => __( '新着情報' ),
      'singular_name'         => __( '新着情報' ),
      'add_new'               => __( '新しく新着情報を書く' ),
      'add_new_item'          => __( '新着情報記事を書く' ),
      'edit_item'             => __( '新着情報記事を編集' ),
      'new_item'              => __( '新しい新着情報記事' ),
      'view_item'             => __( '新着情報記事を見る' ),
      'search_staff'          => __( '新着情報記事を探す' ),
      'not_found'             => __( '新着情報記事はありません' ),
      'not_found_in_trash'    => __( 'ゴミ箱に新着情報記事はありません' ),
      'parent_item_colon'     => ''
    ),
    'public'                  => true,
    'rewrite'                 => true,
    'show_ui'                 => true,
    'supports'                => array ( 'title', 'revisions' ),
    'query_var'               => true,
    'menu_icon'               => 'dashicons-welcome-write-blog',
    // 'taxonomies'              => array ( 'post_tag' ),
    'has_archive'             => true,
    'hierarchical'            => false,
    'menu_position'           => 5,
    'capability_type'         => 'post',
    'show_in_admin_bar'       => true,
    'publicly_queryable'      => true,
  ));
}
add_action ( 'init', 'my_custom_news_post_type' );

function create_cat_taxonomy_newscat () {
  register_taxonomy('newscat', 'news', array (
    'labels'                  => array (
      'name'                  => __( 'カテゴリー' ),
      'menu_name'             => __( 'カテゴリー' ),
      'edit_item'             => __( 'カテゴリ記事を編集' ),
      'all_items'             => __( 'カテゴリー' ),
      'parent_item'           => __( '親カテゴリー' ),
      'add_new_item'          => __( 'カテゴリ記事を書く' ),
      'search_items'          => __( 'カテゴリー' ),
      'singular_name'         => __( 'カテゴリー' ),
      'parent_item_colon'     => __( '親カテゴリー:' ),
    ),
    'show_ui'                 => true,
    'rewrite'                 => array ( 'slug' => 'newscat' ),
    'query_var'               => true,
    'has_archive'             => true,
    'hierarchical'            => true,
    'show_admin_column'       => true,
  ));
}
add_action ( 'init', 'create_cat_taxonomy_newscat', '0' );
