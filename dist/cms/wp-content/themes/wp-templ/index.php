<?php
$thisPageName = 'top';
include(APP_PATH.'libs/head.php');
?>
<link rel="stylesheet" href="<?php echo APP_ASSETS;?>css/lib/aos.min.css">
<link rel="stylesheet" href="<?php echo APP_ASSETS;?>css/lib/overlayscrollbars.min.css">
<link rel="stylesheet" href="<?php echo APP_ASSETS;?>css/page/top.min.css">
</head>
<body id="top" class="top">
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
  <main>
    <div class="top__mainimg">
      <div class="top__mainimg--bg" data-aos="zoom-out" data-aos-delay="300">
        <span class="thumb lazy pc" data-bg="<?php echo APP_ASSETS;?>img/top/img_main.jpg"></span>
        <span class="thumb lazy sp" data-bg="<?php echo APP_ASSETS;?>img/top/img_main_sp.jpg"></span>
      </div>
      <h1 class="top__mainimg--title">
        <img class="lazy pc" data-aos="fade-up" data-aos-delay="800" src="<?php echo createSVG(526,313);?>" data-src="<?php echo APP_ASSETS;?>img/top/txt_main.svg" alt="中小企業から日本を元気にする。">
        <img class="lazy sp" data-aos="fade-up" data-aos-delay="800" src="<?php echo createSVG(260,199);?>" data-src="<?php echo APP_ASSETS;?>img/top/txt_main_sp.svg" alt="中小企業から日本を元気にする。">
      </h1>
      <img class="top__mainimg--ico lazy sp" data-aos="fade-right" data-aos-delay="800" src="<?php echo createSVG(100,94);?>" data-src="<?php echo APP_ASSETS;?>img/top/logo_slider.svg" alt="">
    </div>
    <div class="top__philosophy">
      <img class="top__philosophy--bg lazy pc" src="<?php echo createSVG(648,268);?>" data-src="<?php echo APP_ASSETS;?>img/top/img_chars.png" alt="Corporate Philosophy">
      <img class="top__philosophy--bg lazy sp" src="<?php echo createSVG(371,154);?>" data-src="<?php echo APP_ASSETS;?>img/top/img_chars_sp.png" alt="Corporate Philosophy">
      <div class="wcm top__philosophy--inner">
        <h2 class="top__philosophy--title">Corporate Philosophy</h2>
        <div class="top__philosophy--content">
          <p>現在、日本には４００万社以上の企業があり、そのうち９９％以上が中小企業です。<br>
          日本経済を支える中小企業が元気になれば、日本も必ず元気になります。<br>
          弊社エルネストリンクは「中小企業の右腕」として様々な経営課題を解決する<br class="pc">サービスを展開し、貴社の安定経営をサポートします。</p>
          <a class="cmn_btn" href="<?php echo APP_URL;?>philosophy/"><span>わたしたちにできること</span></a>
        </div>
      </div>
    </div>
    <?php $news = new WP_Query(array(
      'post_type' => 'news',
      'showposts' => -1,
      'no_found_rows' => true,
    ));
    if ($news->have_posts()) : ?>
    <div class="top__news">
      <div class="wcm top__news--inner">
        <div class="top__news--left" data-aos="fade-right">
          <h2 class="title">News</h2>
          <?php /*<a class="more" href="<?php echo APP_URL;?>news/">View All</a>*/ ?>
        </div>
        <div class="top__news--list" data-aos="fade-left">
          <div class="bl_scroll">
            <ul>
              <?php while ($news->have_posts()) : $news->the_post();
              // $thumb = get_the_thumbnail($post->ID);
              $terms = wp_get_post_terms($post->ID,'newscat',array('fields'=>'names'));
              ?>
              <li>
                <div class="content">
                  <?php /*<span class="thumb lazy" data-bg="<?php echo $thumb;?>"></span>*/ ?>
                  <div class="txt">
                    <?php if ($terms) { ?><span class="cat"><?php echo $terms[0];?><em> ｜ </em></span><?php } ?>
                    <span class="date"><?php echo the_time('Y.m.d');?></span>
                    <p class="title"><?php echo get_field('content');?></p>
                  </div>
                </div>
              </li>
              <?php endwhile; ?>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <?php endif; wp_reset_postdata(); ?>
    <div class="top__service">
      <div class="top__service--title" data-aos="zoom-in">
        <p class="title_en"><span>S</span>ervice</p>
        <h2 class="title_jp">エルネストリンクの<br class="sp">サービス</h2>
      </div>
      <ul class="top__service--list">
        <li>
          <div class="content">
            <div class="txt" data-aos="fade-up">
              <div class="box">
                <h3 class="title point01"><span>コスト削減サービス</span></h3>
                <p class="desc">中小企業では営業会社による提案、お知り合い、友人関係での情報となるため偏りがあります。正しい相場、適正な情報を弊社が開示することで大企業並みのボリュームメリットによる削減が可能となります。</p>
                <a class="cmn_btn" href="<?php echo APP_URL;?>service/cost/"><span>サービス詳細</span></a>
              </div>
            </div>
            <div class="img" data-aos="fade-down">
              <div class="c-bg"><div class="arr"></div></div>
              <span class="thumb lazy pc" data-bg="<?php echo APP_ASSETS;?>img/top/img_top01.png"></span>
              <span class="thumb lazy sp" data-bg="<?php echo APP_ASSETS;?>img/top/img_top01_sp.png"></span>
            </div>
          </div>
        </li>
        <li>
          <div class="content">
            <div class="txt" data-aos="fade-up">
              <div class="box">
                <h3 class="title point02"><span>営業動画作成サービス</span></h3>
                <p class="desc">営業動画作成サービスは、自社のサービス内容をイメージと言葉で毎回、一言一句間違えずに相手に伝えることができます。<br>誰が見ても同じ理解が得られるので営業の再現性を向上に繋がります。</p>
                <a class="cmn_btn" href="<?php echo APP_URL;?>service/production/"><span>サービス詳細</span></a>
              </div>
            </div>
            <div class="img" data-aos="fade-down">
              <div class="c-bg"><div class="arr"></div></div>
              <span class="thumb lazy pc" data-bg="<?php echo APP_ASSETS;?>img/top/img_top02.png"></span>
              <span class="thumb lazy sp" data-bg="<?php echo APP_ASSETS;?>img/top/img_top02_sp.png"></span>
            </div>
          </div>
        </li>
        <li>
          <div class="content">
            <div class="txt" data-aos="fade-up">
              <div class="box">
                <h3 class="title point03"><span>MEOサービス</span></h3>
                <p class="desc">MEOとはGoogleマップから直接集客ができる仕組みです。エルネストリンクでは、コスト削減とMEOサービスでの集客を同時に行うことで他ではできない実質無料でのご提案を実現可能にしました。</p>
                <a class="cmn_btn" href="<?php echo APP_URL;?>service/meo/"><span>サービス詳細</span></a>
              </div>
            </div>
            <div class="img" data-aos="fade-down">
              <div class="c-bg"><div class="arr"></div></div>
              <span class="thumb lazy pc" data-bg="<?php echo APP_ASSETS;?>img/top/img_top03.png"></span>
              <span class="thumb lazy sp" data-bg="<?php echo APP_ASSETS;?>img/top/img_top03_sp.png"></span>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </main>
</div>
<?php include(APP_PATH.'libs/footer.php'); ?>
<script src="<?php echo APP_ASSETS;?>js/lib/aos.min.js"></script>
<script src="<?php echo APP_ASSETS;?>js/lib/overlayscrollbars.min.js"></script>
<script src="<?php echo APP_ASSETS;?>js/page/top.min.js"></script>
</body>
</html>
