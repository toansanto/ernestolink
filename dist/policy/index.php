<?php
$thisPageName = 'policy';
include_once(dirname(__DIR__) . '/app_config.php');
include(APP_PATH.'libs/head.php');
?>
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/policy.min.css">
</head>
<body id="policy" class="policy">
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
    <main>
        <div class="cmn_hd">
            <div class="cmn_hd__inner">
                <div class="cmn_hd__bg">
                    <span
                        class="thumb lazy pc"
                        data-bg="<?php echo APP_ASSETS;?>img/policy/img_main.jpg"></span>
                    <span
                        class="thumb lazy sp"
                        data-bg="<?php echo APP_ASSETS;?>img/policy/img_main-sp.jpg"></span>
                </div>
                <div class="wcm cmn_hd__title">
                    <span class="cmn_hd__title--en">PRIVACY POLICY</span>
                    <h1 class="cmn_hd__title--jp">個人情報保護方針</h1>
                </div>
            </div>
        </div>
        <div class="breadcrumbs">
            <ul class="wcm">
                <li>
                    <a href="<?php echo APP_URL;?>">TOP</a>
                </li>
                <li>
                    <span>個人情報保護方針</span></li>
            </ul>
        </div>
        <div class="content inner">
            <div class="content__title">
                株式会社エルネストリンク（以下、「当社」）は、サービスのご提供にあたり、お客様の個人情報をお預かりしております。当社は、個人情報を保護し、お客様に更なる信頼性と安心感をご提供できるように努めると共に、個人情報に関する法令を遵守し、個人情報の適切な取り扱いを致します。
            </div>
            <ul class="content__info">
                <li>
                    <h4>個人情報の定義</h4>
                    <p>「個人情報」とは、生存する個人に関する情報であって、当該情報に含まれる氏名、生年月日その他の記述等により特定の個人を識別することができるもの、及び他の情報と容易に照合することができ、それにより特定の個人を識別することができることとなるものを言います。
                    </p>
                </li>
                <li>
                    <h4>個人情報の取得について</h4>
                    <p>当社は、偽りその他不正の手段によらず適正に個人情報を取得致します。</p>
                </li>
                <li>
                    <h4>アクセス解析ツールについて</h4>
                    <p>当サイトでは、Googleによるアクセス解析ツール「Googleアナリティクス」を利用しています。<br>
                        このGoogleアナリティクスはトラフィックデータの収集のためにCookieを使用しています。<br>
                        このトラフィックデータは匿名で収集されており、個人を特定するものではありません。<br>
                        この機能はCookieを無効にすることで収集を拒否することが出来ますので、お使いのブラウザの設定をご確認ください。</p>
                </li>
                <li>
                    <h4>個人情報の利用について</h4>
                    <p>当社は、個人情報を以下の利用目的の達成に必要な範囲内で利用致します。<br>
                        以下に定めのない目的で個人情報を利用する場合、あらかじめご本人の同意を得た上で行ないます。</p>
                    <div class="note">
                        <p>・お申し込み・ご質問に対する回答</p>
                        <p>・当社サービスに関する情報提供</p>
                        <p>・ウェブサイトの改善</p>
                    </div>
                </li>
                <li>
                    <h4>個人情報の安全管理について</h4>
                    <p>当社は、取り扱う個人情報の漏洩がないように、個人情報の安全管理のために必要かつ適切な措置を講じます。</p>
                </li>
                <li>
                    <h4>個人情報の第三者提供について</h4>
                    <p>当社は、下記の場合を除いてはお客様の断りなく第三者に個人情報を開示・提供することはいたしません。</p>
                    <div class="note">
                        <p>・法令に基づく場合、及び国の機関若しくは地方公共団体又はその委託を受けた者が法令の定める事務を遂行することに対して協力する必要がある場合</p>
                        <p>・人の生命、身体又は財産の保護のために必要がある場合であって、本人の同意を得ることが困難である場合</p>
                        <p>・商品、関連書類、ご案内の配送・郵送をするために、配送会社に配送委託する場合</p>
                    </div>
                </li>
                <li>
                    <h4>組織・体制</h4>
                    <p class="spec">当社は、株式会社エルネストリンク代表取締役 谷岡 遼を個人情報管理責任者とし、個人情報の適正な管理及び継続的な改善を実施致します。
                    </p>
                </li>
                <li>
                    <h4>本方針の変更</h4>
                    <p>本方針の内容は変更されることがあります。変更後の本方針については、<br>
                        当社が別途定める場合を除いて当サイトに掲載した時から効力を生じるものとします。</p>
                </li>
                <li>
                    <p>【制定日】2020年7月30日</p>
                </li>
            </ul>
        </div>
    </main>
</div>
<?php include(APP_PATH.'libs/footer.php'); ?>
</body>
</html>
