<?php
$thisPageName = 'meo';
include_once(dirname(__DIR__) . '/../app_config.php');
include(APP_PATH.'libs/head.php');
?>
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/meo.min.css">
</head>
<body id="meo" class="meo">
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
    <main>
        <div class="cmn_hd">
            <div class="cmn_hd__inner">
                <div class="cmn_hd__bg">
                    <span
                        class="thumb lazy pc"
                        data-bg="<?php echo APP_ASSETS;?>img/meo/img_main.jpg"></span>
                    <span
                        class="thumb lazy sp"
                        data-bg="<?php echo APP_ASSETS;?>img/meo/img_main_sp.jpg"></span>
                </div>
                <div class="wcm cmn_hd__title">
                    <span class="cmn_hd__title--en">MAP ENGINE OPTIMIZATION</span>
                    <h1 class="cmn_hd__title--jp">MEO（マップエンジン最適化）<br class="sp">サービス</h1>
                </div>
            </div>
        </div>
        <div class="breadcrumbs">
            <ul class="wcm">
                <li>
                    <a href="<?php echo APP_URL;?>">TOP</a>
                </li>
                <li class="mar">サービス一覧</li>
                <li>
                    <span>MEO（マップエンジン最適化）サービス</span></li>
            </ul>
        </div>
        <div class="meo__content">
            <div class="content01">
                <div class="content01__title inner">
                    <span class="content01__title__ico"><img src="<?php echo APP_ASSETS;?>img/philosophy/icon.png" alt="icon"></span>
                    <h4>実店舗への来店見込みの<br class="sp">高いユーザーに訴求</h4>
                    <p>MEOとはGoogleマップから直接集客ができる仕組みです。エルネストリンクでは、コスト削減とMEOサービスでの集客を同時に行うことで他ではできない実質無料でのご提案を実現可能にしました。</p>
                    <div class="content01__title__img"><img
                        src="<?php echo APP_ASSETS;?>img/meo/text02.svg"
                        alt="なぜMEOがいいの？"
                        class="pc"><img
                        src="<?php echo APP_ASSETS;?>img/meo/text02-sp.svg"
                        alt="なぜMEOがいいの？"
                        class="sp"></div>
                </div>
            </div>
            <div class="content02">
                <div class="inner">
                    <h4>成果が出るまで費用０円、<br class="sp">完全成功報酬制MEOサービス。<br>
                        初期費用0円でGoogleマップ<br class="sp">上位に表示されるまで費用は一切<br class="sp">かかりません。
                    </h4>
                    <div class="content02__content">
                        <div class="left"><img
                            src="<?php echo APP_ASSETS;?>img/meo/text01.svg"
                            alt="成果が出るまで費用０円、完全成功報酬制MEOサービス。"
                            class="pc"><img
                            src="<?php echo APP_ASSETS;?>img/meo/text01-sp.svg"
                            alt="成果が出るまで費用０円、完全成功報酬制MEOサービス。"
                            class="sp"></div>
                        <div class="right">
                            <span>お店やサービスを探す時の<br>
                                ユーザーの行動パターン</span>
                            <div>例：「カフェを探す」</div>
                            <ul>
                                <li>この付近にはどんなカフェがあるかな？<br>
                                    「新大阪&nbsp&nbsp&nbspカフェ」で検索<em><img src="<?php echo APP_ASSETS;?>img/meo/arrow.png" alt="arrow"></em>
                                </li>
                                <li>地図のすぐ下に結果が表示<br>
                                    良さそうなカフェを選ぶ<em><img src="<?php echo APP_ASSETS;?>img/meo/arrow.png" alt="arrow"></em>
                                </li>
                                <li>ルート検索やウェブサイトを見て<br>
                                    カフェに向かったり予約をする</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content03">
                <a href="<?php echo APP_URL;?>contact/?opt=3">
                    <div class="content03__button">
                        MEOサービスについての<br>
                        お問い合わせはこちら
                    </div>
                </a>
            </div>
            <div class="content04">
                <div class="content04__iwsi"><img src="<?php echo APP_ASSETS;?>img/meo/iwsi.png" alt="このような課題を解決します"></div>
                <div class="content04__content">
                    <div class="tit">このような課題を解決します</div>

                    <ul class="content04__talk inner">
                        <li>
                            <div class="up">
                                <span>課 題</span>
                                <p>MEOという言葉はよく聞くけど、どうしたら良いかわからない。</p>
                            </div>
                            <div class="down">
                                <span>解 決</span>
                                <p>「地域名＋○○（業種・サービス）」のキーワードで検索した際に表示されるので<br class="pc">
                                    実店舗に行く可能性が高いユーザーに訴求することが可能です。</p>
                            </div>
                        </li>
                        <li>
                            <div class="up">
                                <span>課 題</span>
                                <p>グーグルマイビジネスはよく聞くけど何も設定していないのですが…。</p>
                            </div>
                            <div class="down">
                                <span class="spec">解 決</span>
                                <p>Googleマイビジネスを利用して店舗情報を充実させるための設定も行います。<br>
                                    「電話をかけたい」、「経路を確認したい」、「写真を見たい」、「口コミを見たい」など、<br class="pc">
                                    これら全ての行動を検索画面上で完結させることができます。</p>
                            </div>
                        </li>
                        <li>
                            <div class="up">
                                <span>課 題</span>
                                <p>MEOだけで本当に効果が出るのか不安。</p>
                            </div>
                            <div class="down">
                                <span class="spec">解 決</span>
                                <p>検索上位に上がってきても口コミや写真などの情報が充実していないと<br>
                                    ユーザーから選んでもらえません。<br>
                                    私たちはユーザーから選ばれ、集客率がアップする方法を一緒に考えます。</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </main>
</div>
<?php
include(APP_PATH.'libs/service.php');
include(APP_PATH.'libs/footer.php');
?>
</body>
</html>