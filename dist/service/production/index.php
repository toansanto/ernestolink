<?php
$thisPageName = 'production';
include_once(dirname(__DIR__) . '/../app_config.php');
include(APP_PATH.'libs/head.php');
?>
<link
    rel="stylesheet"
    href="<?php echo APP_ASSETS ?>css/page/production.min.css">
</head>
<body id="production" class="production">
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
    <main>
        <div class="cmn_hd">
            <div class="cmn_hd__inner">
                <div class="cmn_hd__bg">
                    <span
                        class="thumb lazy pc"
                        data-bg="<?php echo APP_ASSETS;?>img/production/img_main.jpg"></span>
                    <span
                        class="thumb lazy sp"
                        data-bg="<?php echo APP_ASSETS;?>img/production/img_main_sp.jpg"></span>
                </div>
                <div class="wcm cmn_hd__title">
                    <span class="cmn_hd__title--en">SALES MOVIE</span>
                    <h1 class="cmn_hd__title--jp">営業動画制作<br class="sp">サービス</h1>
                </div>
            </div>
        </div>
        <div class="breadcrumbs">
            <ul class="wcm">
                <li>
                    <a href="<?php echo APP_URL;?>">TOP</a>
                </li>
                <li class="mar">サービス一覧</li>
                <li>
                    <span>営業動画制作サービス</span></li>
            </ul>
        </div>
        <div class="production__content">
            <div class="content01">
                <div class="content01__title inner">
                    <span class="content01__title__ico"><img src="<?php echo APP_ASSETS;?>img/philosophy/icon.png" alt="icon"></span>
                    <h4>営業のDXを実現するための<br class="sp">手段としての動画という選択</h4>
                    <p>営業動画作成サービスは、自社のサービス内容をイメージと言葉で毎回、一言一句間違えずに相手に伝えることができます。<br class="pc">
                        誰が見ても同じ理解が得られるので営業の再現性を向上に繋がります。</p>
                </div>
            </div>
            <div class="content02">
                <div class="content02__content inner">
                    <div class="content02__content__img">
                        <img
                            src="<?php echo APP_ASSETS;?>img/production/text01.svg"
                            alt="営業のDXを実現するための手段としての動画という選択"
                            class="pc">
                        <img
                            src="<?php echo APP_ASSETS;?>img/production/text01-sp.svg"
                            alt="営業のDXを実現するための手段としての動画という選択"
                            class="sp">
                    </div>
                    <ul class="content02__content__point">
                        <li>
                            <div class="img-p">
                                <img
                                    src="<?php echo APP_ASSETS;?>img/production/p01.png"
                                    alt="紙だった営業資料が1クリックで再生">
                            </div>
                            <div class="text">
                                紙だった営業資料が<br>
                                <span>1クリック</span>で再生
                            </div>
                        </li>
                        <li>
                            <div class="img-p">
                                <img
                                    src="<?php echo APP_ASSETS;?>img/production/p02.png"
                                    alt="複数資料を使わずに1つの動画で説明が完結">
                            </div>
                            <div class="text">
                                複数資料を使わずに<br>
                                <span>1つの動画で説明が完結</span>
                            </>
                        </li>
                        <li>
                            <div class="img-p">
                                <img
                                    src="<?php echo APP_ASSETS;?>img/production/p03.png"
                                    alt="ウェブ上でいつでも閲覧できてスムーズ営業">
                            </div>
                            <div class="text">
                                ウェブ上で<span>いつでも閲覧</span><br>
                                できてスムーズ営業
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="content03">
                <a href="<?php echo APP_URL;?>contact/?opt=2">
                    <div class="content03__button">
                        営業動画制作サービスについての<br>
                        お問い合わせはこちら
                    </div>
                </a>
            </div>
            <div class="content04">
                <div class="content04__iwsi"><img src="<?php echo APP_ASSETS;?>img/meo/iwsi.png" alt="このような課題を解決します"></div>
                <div class="content04__content">
                    <div class="tit">このような課題を解決します</div>

                    <ul class="content04__talk inner">
                        <li>
                            <div class="up">
                                <span>課 題</span>
                                <p>営業担当が自社のサービス内容をうまく伝えることができない。</p>
                            </div>
                            <div class="down">
                                <span>解 決</span>
                                <p>営業動画制作サービスはサービスのポイントをしっかりとまとめた動画に仕上がります。<br class="pc">
                                    一言一句、間違えることなく相手に毎回同じ理解が得られるため営業力アップに繋がります。</p>
                            </div>
                        </li>
                        <li>
                            <div class="up">
                                <span>課 題</span>
                                <p>クライアントを紹介する際にどんな会社か伝えるのが難しい。</p>
                            </div>
                            <div class="down">
                                <span class="spec">解 決</span>
                                <p>クライアント情報を自分の中で整理して伝えるのが難しい方は<br class="pc">
                                    この動画を見せることで要点を素早く相手に伝えることができます。<br>
                                    イメージを伝えることや説明が苦手な方でも同一品質で説明が可能になります。</p>
                            </div>
                        </li>
                        <li>
                            <div class="up">
                                <span>課 題</span>
                                <p>会社の説明をする際にスマートにわかりやすく改善したい。</p>
                            </div>
                            <div class="down">
                                <span class="spec">解 決</span>
                                <p>動画にすることで紙だった営業資料が動画にすることで1クリックで説明が完了します。<br>
                                    複数資料を使わずに動画＋補足説明で完結。<br>
                                    ウェブ上にアップロードしておけば24時間いつでも閲覧可能です。</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </main>
</div>
<?php
include(APP_PATH.'libs/service.php');
include(APP_PATH.'libs/footer.php');
?>
</body>
</html>