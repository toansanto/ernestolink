<?php
$thisPageName = 'cost';
include_once(dirname(__DIR__) . '/../app_config.php');
include(APP_PATH.'libs/head.php');
?>
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/cost.min.css">
</head>
<body id="cost" class="cost">
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
    <main>
        <div class="cmn_hd">
            <div class="cmn_hd__inner">
                <div class="cmn_hd__bg">
                    <span
                        class="thumb lazy pc"
                        data-bg="<?php echo APP_ASSETS;?>img/cost/img_main.jpg"></span>
                    <span
                        class="thumb lazy sp"
                        data-bg="<?php echo APP_ASSETS;?>img/cost/img_main_sp.jpg"></span>
                </div>
                <div class="wcm cmn_hd__title">
                    <span class="cmn_hd__title--en">COST REDUCTION</span>
                    <h1 class="cmn_hd__title--jp">コスト削減サービス</h1>
                </div>
            </div>
        </div>
        <div class="breadcrumbs">
            <ul class="wcm">
                <li>
                    <a href="<?php echo APP_URL;?>">TOP</a>
                </li>
                <li class="mar">サービス一覧</li>
                <li>
                    <span>コスト削減サービス</span></li>
            </ul>
        </div>
        <div class="cost__content">
            <div class="content01">
                <div class="content01__title inner">
                    <span class="content01__title__ico"><img src="<?php echo APP_ASSETS;?>img/philosophy/icon.png" alt="icon"></span>
                    <h4>御社の経営を安定経営に変える<br class="sp">３Ａコスト削減サービス</h4>
                    <p>大企業と中小企業では商談の数、質共に圧倒的に乖離があります。<br class="pc">
                        大企業では各項目、商材毎に担当者がおり情報を精査する者がいます。自動的にコンペ方式で経費削減を実現しています。<br class="pc">
                        一方中小企業では営業会社による提案、お知り合い、友人関係での情報となるため偏りがあります。<br class="pc">
                        正しい相場、適正な情報を弊社が開示することで大企業並みのボリュームメリットによる削減が可能となります。</p>
                </div>
            </div>
            <div class="content02">
                <div class="inner">
                    <div class="content02__content">
                        <div class="video-box">
                            <div class="left"><img
                                src="<?php echo APP_ASSETS;?>img/cost/text01.svg"
                                alt="まずはこの動画をご覧ください。"
                                class="pc"><img src="<?php echo APP_ASSETS;?>img/cost/text01-sp.svg" alt="iwsi" class="sp"></div>
                            <div class="right">
                                <div class="vidframe">
                                    <video playsinline="playsinline" controls="controls" poster="<?php echo APP_ASSETS;?>img/cost/thumb_vid.jpg">
                                        <source
                                            type="video/mp4"
                                            src="<?php echo APP_ASSETS;?>img/cost/costcutmovie.mp4">
                                    </video>
                                </div>
                            </div>
                        </div>
                        <div class="img-box">
                            <img
                                src="<?php echo APP_ASSETS;?>img/cost/text02.svg"
                                alt="過去1000件以上のコスト削減データを活用した「安心」「安全」「安価」の３Ａコスト削減で経費削減を実現します。"
                                class="pc">
                            <img
                                src="<?php echo APP_ASSETS;?>img/cost/text02-sp.svg"
                                alt="過去1000件以上のコスト削減データを活用した「安心」「安全」「安価」の３Ａコスト削減で経費削減を実現します。"
                                class="sp">
                        </div>
                        <h4>過去1000件以上のコスト削減データを<br class="sp">活用した<br class="pc">
                            「安心」「安全」「安価」の<br class="sp">３Ａコスト削減で経費削減を実現します。</h4>
                    </div>
                </div>
            </div>
            <div class="content05 inner">
                <div class="box">
                    <div class="left">
                        <img
                            src="<?php echo APP_ASSETS;?>img/cost/text03.svg"
                            alt="簡単に利益を増やすこと"
                            class="pc">
                    </div>
                    <div class="right">
                        <span class="text01">コスト削減は最も確実で</span>
                        <h5>簡単に利益<br class="pc">を増やすこと</h5>
                        <span class="text02">のできる取り組みです。</span>
                        <p>リスク０のコスト削減で「経費削減」<br class="pc">
                            「キャッシュフロー改善」を実現した<br class="pc">
                            企業様をご紹介します。</p>
                        <img
                            src="<?php echo APP_ASSETS;?>img/cost/text03-sp.svg"
                            alt="簡単に利益を増やすこと"
                            class="sp">
                        <a href="<?php echo APP_URL;?>case/">
                            <div class="button">
                                導入事例・実績はこちら<img src="<?php echo APP_ASSETS;?>img/cost/ico.png" alt="ico" class="ico">
                            </div>
                        </a>
                    </div>
                </div>
                <div class="box box01">
                    <div class="right">
                        <span class="text01">エルネストリンクは</span>
                        <h5>完全成功<br class="pc">報酬型</h5>
                        <span class="text02">でコスト削減を実現可能。</span>
                        <p>過去１０００件以上のコスト削減データを<br class="pc">
                            活用した「安心」「安全」「安価」の３Ａ<br class="pc">
                            コスト削減で経費削減を実現します！</p>
                        <img
                            src="<?php echo APP_ASSETS;?>img/cost/text04-sp.svg"
                            alt="完全成功報酬型"
                            class="sp">
                        <a href="https://www.chatwork.com/ernesto-link" target="_blank">
                            <div class="button">
                                まずはご相談ください<img src="<?php echo APP_ASSETS;?>img/cost/ico.png" alt="ico" class="ico">
                            </div>
                        </a>
                    </div>
                    <div class="left">
                        <img src="<?php echo APP_ASSETS;?>img/cost/text04.svg" alt="完全成功報酬型" class="pc">
                    </div>
                </div>
                <ul class="point">
                    <li>
                        <div class="img-p">
                            <img
                                src="<?php echo APP_ASSETS;?>img/production/p01.png"
                                alt="事前に貴社のコスト状況をチェック">
                        </div>
                        <div class="text">事前に貴社の<br class="pc">
                            コスト状況をチェック
                        </div>
                        <p>明細をお預かりし、現状のちらばった情報を管理シートを使用して可視化します。</p>
                    </li>
                    <li>
                        <div class="img-p">
                            <img
                                src="<?php echo APP_ASSETS;?>img/production/p02.png"
                                alt="過去1000件のビッグデータで分析">
                        </div>
                        <div class="text">過去1000件の<br class="pc">
                            ビッグデータで分析
                        </div>
                        <p>過去の事例、実績から最安値を算出し、<br class="sp">比較することで削減可能額の算出が<br class="sp">可能となります。</p>
                    </li>
                    <li>
                        <div class="img-p">
                            <img src="<?php echo APP_ASSETS;?>img/production/p03.png" alt="シミュレーションで見える化">
                        </div>
                        <div class="text">シミュレーションで<br class="pc">
                            見える化
                        </div>
                        <p>分析結果を一覧表にし今後のコスト効率も<br class="sp">分かりやすく纏めます。その後管理の為に<br class="sp">シートをデータでお渡ししております。</p>
                    </li>
                </ul>
            </div>
            <div class="content03">
                <a href="<?php echo APP_URL;?>contact/?opt=1">
                    <div class="content03__button">
                        経費削減試算〈無料申込み〉はこちら
                    </div>
                </a>
            </div>
            <div class="content04">
                <div class="content04__iwsi"><img src="<?php echo APP_ASSETS;?>img/meo/iwsi.png" alt="このような課題を解決します"></div>
                <div class="content04__content">
                    <div class="tit">このような課題を解決します</div>
                    <ul class="content04__talk inner">
                        <li>
                            <div class="up">
                                <span>課 題</span>
                                <p>会社の成長とともに固定費が上がっているが、見直しができていない。</p>
                            </div>
                            <div class="down">
                                <span>解 決</span>
                                <p>まずは現状の経費の見える化のお手伝いをさせていただき、過去のデータとの比較、<br class="pc">
                                    及び交渉をいたしますので、ご担当の方のお時間を奪うことなくコスト削減が可能。</p>
                            </div>
                        </li>
                        <li>
                            <div class="up">
                                <span>課 題</span>
                                <p>利益はでているが、内部留保が思うように増えない。</p>
                            </div>
                            <div class="down">
                                <span class="spec">解 決</span>
                                <p>特にお借入をされている法人さまに多い悩みですが、<br class="pc">
                                    内部留保は税引き後利益からしか捻出できません。<br class="pc">
                                    経費削減をすれば理論上は直接純利益となり、キャッシュフロー改善が可能になります。</p>
                            </div>
                        </li>
                        <li>
                            <div class="up">
                                <span>課 題</span>
                                <p>金融機関からの借入があり、返済額が負担になっている。</p>
                            </div>
                            <div class="down">
                                <span class="spec">解 決</span>
                                <p>まずは経費削減をして月々の返済の財源を捻出することにより、<br class="pc">
                                    返済負担を軽減できる事例は数多くございます。<br class="pc">
                                    また、最終手段として「リスケ」のお手伝いをさせていただくこともございます。</p>
                            </div>
                        </li>
                        <li>
                            <div class="up">
                                <span>課 題</span>
                                <p>各種リース等の管理担当者が代わり、把握ができず、管理コストが上がっている。</p>
                            </div>
                            <div class="down">
                                <span class="spec01">解 決</span>
                                <p>知識や情報がない中での把握はかなり難しく、各販売元の方に任せてしまうと<br class="pc">
                                    いつの間にか費用が高くなりがちです。弊社専門のサポートチームによる管理表作成や<br class="pc">
                                    更新月等のお知らせなど、管理担当者様のご負担を少しでも低減させるサポートを<br class="pc">
                                    させていただいております。</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="content06">
                <div class="content06__performance--title">
                    <p class="title_en">
                        <span>C</span>ONSULTING FEE</p>
                    <h2 class="title_jp">コスト削減費用は、お客様に<br class="sp">ノーリスクの料金システム<br>
                        初年度のコスト削減額の中から<br class="sp">８０％を報酬としていただきます</h2>
                </div>
                <div class="img inner">
                    <img
                        src="<?php echo APP_ASSETS;?>img/cost/text05.svg"
                        alt="コスト削減費用は、お客様にノーリスクの料金システム"
                        class="pc">
                    <img
                        src="<?php echo APP_ASSETS;?>img/cost/text05-sp.svg"
                        alt="コスト削減費用は、お客様にノーリスクの料金システム"
                        class="sp">
                </div>
            </div>
            <div class="content07">
                <div class="content06__performance--title">
                    <p class="title_en">
                        <span>F</span>LOW</p>
                    <h2 class="title_jp">コスト削減サービスの流れ</h2>
                </div>
                <ul class="step-box">
                    <li>
                        <div class="step-num">STEP.<span>01</span></div>
                        <div class="content">
                            <h5>ご訪問・ヒアリング（テレビ会議を含む）</h5>
                            <p>ご訪問の後、弊社概要をご説明させていただき、御社の経費削減の課題を丁寧に伺います。弊社提案にご納得いただけましたら、コンサルティング契約を締結させていただき、経費削減プロジェクトがスタートします（ヒアリングはテレビ会議などの非対面方式も可能です）。</p>
                        </div>
                    </li>
                    <li>
                        <div class="step-num">STEP.<span>02</span></div>
                        <div class="content">
                            <h5>削減可能な経費明細の取得サポート</h5>
                            <p>ヒアリングで伺った削減可能な予想項目と、御社が削減したい項目のすり合わせを行います。クライアント様からは経費明細を出していただいておりますが、もしも明細が手元にない場合は、契約元から取り寄せる代行もさせていただきます。</p>
                        </div>
                    </li>
                    <li>
                        <div class="step-num">STEP.<span>03</span></div>
                        <div class="content">
                            <h5>専用フォーマットへの入力・可視化</h5>
                            <p>いただいた明細をもとに、こちらで弊社の管理ツールに入力します。煩雑な明細を見える化できるので、「現在の経費がどれくらいかかっているか」「リースがどれくらい残っているか」などが一目でわかるようになります。後々の管理ツールとしてもご活用いただけます。</p>
                        </div>
                    </li>
                    <li>
                        <div class="step-num">STEP.<span>04</span></div>
                        <div class="content">
                            <h5>過去実績データとの比較</h5>
                            <p>入力から２週間ほどを目安に、弊社で過去１０００件以上の経費削減実績データと比較し、「現在の使い方にどういうムダがあるか」
                                「どのくらいの経費削減ができるか」 「生産性の向上をはかれるか」などを算出いたします。</p>
                        </div>
                    </li>
                    <li>
                        <div class="step-num">STEP.<span>05</span></div>
                        <div class="content">
                            <h5>削減試算結果のご提示</h5>
                            <p>比較した内容を提示させていただき、試算結果を提示します。「なぜそのような試算になったか」「どうすれば効率のいい使い方ができるか」などの説明も致しますので、ご納得の上で経費削減に取り組んでいただけます。</p>
                        </div>
                    </li>
                    <li>
                        <div class="step-num">STEP.<span>06</span></div>
                        <div class="content">
                            <h5>各項目ごとに交渉を実施</h5>
                            <p>経費削減の方向性が決まり次第、契約ごとにリバースオークションを実施し、契約元を検討します。また、契約元を変更したくないご意思がある場合は、リバースオークションで算出された最安値のエビデンスをもとに、弊社で契約元との交渉をいたします。</p>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="content08">
                <div class="content08__performance--title">
                    <p class="title_en">
                        <span>P</span>ERFORMANCE</p>
                    <h2 class="title_jp">これまでの活動実績</h2>
                </div>
                <ul class="content08__box inner">
                    <li class="content08__box__info">
                        <div class="content08__box__info__img img1">
                            <p class="up">削減成功率</p>
                            <p class="down">
                                <em>92</em>
                                <span>%</span></p>
                        </div>
                        <h5>〈人件費以外は下げられます〉</h5>
                        <p>どんな企業でも必ず削減できるところがあります。時代の変化と共に生まれる「経費のムダ」を最新のビッグデータの活用で削減。過去1000社以上の企業様の実績で削減成功率は92%。</p>
                    </li>
                    <li class="content08__box__info">
                        <div class="content08__box__info__img img2">
                            <p class="up">導入件数</p>
                            <p class="down">
                                <em>1200</em>
                                <span>件以上</span></p>
                        </div>
                        <h5>〈モノを良くした上で安くする〉</h5>
                        <p>導入企業様や関連企業様からのご紹介だけで全国から依頼をいただいております。<br class="sp">「モノを良くした上で安くする」の考え方で、安全に企業のコストを削減し、安定経営のお手伝いをさせていただいております。</p>
                    </li>
                    <li class="content08__box__info">
                        <div class="content08__box__info__img img3">
                            <p class="up">継続削減率</p>
                            <p class="down">
                                <em>81</em>
                                <span>%</span></p>
                        </div>
                        <h5>〈削減して終わりではありません〉</h5>
                        <p>コスト削減後、グループウェアを組ませていただき、専任の弊社サポート担当者がアフターフォローをさせていただきます。<br class="sp">これにより、コスト削減した項目の管理コストも継続して削減できます。</p>
                    </li>
                    <li class="content08__box__info">
                        <div class="content08__box__info__img img4">
                            <p class="up">パートナー企業</p>
                            <p class="down">
                                <em>57</em>
                                <span>社</span></p>
                        </div>
                        <h5>〈新規営業はしていません〉</h5>
                        <p>大手税理士法人、社労事務所、金融機関などの有資格者の方々と提携して事業を行っております。キャッシュフローの専門家からの信頼も高く、数多くのご紹介をいただいております。</p>
                    </li>
                </ul>
            </div>
        </div>
    </main>
</div>
<?php
include(APP_PATH.'libs/service.php');
include(APP_PATH.'libs/footer.php');
?>
</body>
</html>
