<div class="cmn_download">
  <div class="cmn_download__inner">
    <div class="cmn_download__txt">
      <span>Download</span>
      <p>エルネストリンクの説明資料を<br>ダウンロードすることができます</p>
    </div>
    <div class="cmn_download__img">
      <span class="thumb lazy" data-bg="<?php echo APP_ASSETS;?>img/common/footer/img_download.jpg"></span>
      <a class="cmn_btn" href="<?php echo APP_URL;?>pdf/ernesto-link-document.pdf" target="_blank"><span>ダウンロードはこちら</span></a>
    </div>
  </div>
</div>
<div class="contactBox">
  <div class="contactBox__bg">
    <span class="thumb lazy pc" data-bg="<?php echo APP_ASSETS;?>img/common/footer/bg_contact.jpg"></span>
    <span class="thumb lazy sp" data-bg="<?php echo APP_ASSETS;?>img/common/footer/bg_contact_sp.jpg"></span>
  </div>
  <div class="wcm contactBox__inner">
    <p class="contactBox__title">〈お問い合わせ・ご相談はお気軽に〉</p>
    <p class="contactBox__desc">コスト削減を考えている企業担当者様からのご相談はお電話、メール、チャットなど<br class="pc">様々なご連絡方法に対応しております。まずはお気軽にご連絡ください。</p>
    <div class="contactBox__list">
      <div class="contactBox__list--left">
        <div class="contactBox__tel">
          <a href="tel:06-6910-1334"><img class="lazy" src="<?php echo createSVG(312,37);?>" data-src="<?php echo APP_ASSETS;?>img/common/footer/txt_tel.svg" alt="06-6910-1334"></a>
          <span>受付時間／<em>10:00〜19:00</em></span>
        </div>
        <ul class="contactBox__btns">
          <li><a class="btn_contact" href="<?php echo APP_URL;?>contact/"><span>メールでのお問い合わせ</span></a></li>
          <li><a class="btn_chatwork" href="https://www.chatwork.com/ernesto-link/"><img class="lazy" src="<?php echo createSVG(118,24);?>" data-src="<?php echo APP_ASSETS;?>img/common/icon/ico_chatwork.svg" alt="Chatwork"><span>でのお問い合わせ</span></a></li>
        </ul>
      </div>
      <div class="contactBox__list--right">
        <ul class="contactBox__staff">
          <li>
            <img class="lazy pc" src="<?php echo createSVG(150,150);?>" data-src="<?php echo APP_ASSETS;?>img/common/footer/img_staff01.png" alt="サポート担当者 仲田 真弓">
            <img class="lazy sp" src="<?php echo createSVG(130,130);?>" data-src="<?php echo APP_ASSETS;?>img/common/footer/img_staff01_sp.png" alt="サポート担当者 仲田 真弓">
          </li>
          <li>
            <img class="lazy pc" src="<?php echo createSVG(150,150);?>" data-src="<?php echo APP_ASSETS;?>img/common/footer/img_staff02.png" alt="サポート担当者 木田 光治">
            <img class="lazy sp" src="<?php echo createSVG(130,130);?>" data-src="<?php echo APP_ASSETS;?>img/common/footer/img_staff02_sp.png" alt="サポート担当者 木田 光治">
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<footer id="footer" class="footer">
  <div class="footer__frame">
    <a id="btn_gotop" class="footer__gotop" href="javascript:;"></a>
    <div class="wcm footer__inner">
      <div class="footer__left">
        <p class="footer__left--desc">インフラ・コスト削減のことなら<br class="sp">エルネストリンクにお任せください。</p>
        <a class="footer__left--logo" href="<?php echo APP_URL;?>"><img class="lazy" src="<?php echo createSVG(300,59);?>" data-src="<?php echo APP_ASSETS;?>img/common/header/logo.svg" alt="Ernesto Link"></a>
        <p class="footer__left--address">
          <strong>株式会社エルネストリンク</strong>
          〒540-0029<br>
          大阪府大阪市中央区本町橋2-17 AXIS内本町ビル9F<br>
          TEL.<a href="tel:06-6910-1334">06-6910-1334</a>
        </p>
      </div>
      <div class="footer__right">
        <p class="footer__right--title"><span>Sitemap</span></p>
        <ul class="footer__right--menu">
          <li class="fw"><a href="<?php echo APP_URL;?>">エルネストリンクTOP</a></li>
          <li>
            <a href="<?php echo APP_URL;?>philosophy/">企業理念</a>
            <a href="<?php echo APP_URL;?>case/">導入事例</a>
            <a href="<?php echo APP_URL;?>company/">会社紹介</a>
          </li>
          <li>
            <a href="<?php echo APP_URL;?>service/cost/">コスト削減サービス</a>
            <a href="<?php echo APP_URL;?>service/production/">営業動画作成サービス</a>
            <a href="<?php echo APP_URL;?>service/meo/">MEOサービス</a>
          </li>
          <li>
            <a href="<?php echo APP_URL;?>contact/">お問い合わせ</a>
            <a href="https://www.chatwork.com/ernesto-link">チャットによる無料個別相談</a>
            <a href="<?php echo APP_URL;?>policy/">個人情報保護方針</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <p id="copyright" class="footer__copyright">&copy; ERNESTO LINK</p>
</footer>

<script src="<?php echo APP_ASSETS;?>js/common.min.js"></script>
