<?php $list_cat = get_terms(array(
  'taxonomy' => 'casecat',
  'hide_empty' => true,
));
if ($list_cat) { ?>
<div class="case__content--right">
  <div class="title">
    <span>Category</span>
    <p>業種から事例を見る</p>
  </div>
  <ul class="lst_cat">
    <li><a href="<?php echo APP_URL;?>case/">すべての事例を見る</a></li>
    <?php foreach ($list_cat as $item) { ?>
    <li><a href="<?php echo APP_URL.'casecat/'.$item->slug;?>"><?php echo $item->name;?></a></li>
    <?php } ?>
  </ul>
</div>
<?php } ?>
