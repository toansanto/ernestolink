<div class="cmn_service">
  <div class="cmn_service__title">
    <p class="title_en"><span>S</span>ervice</p>
    <p class="title_jp">エルネストリンクの<br class="sp">サービス</p>
  </div>
  <div class="cmn_service__content">
    <div class="cmn_service__bg">
      <span class="thumb lazy pc" data-bg="<?php echo APP_ASSETS;?>img/common/other/bg_service.jpg"></span>
      <span class="thumb lazy sp" data-bg="<?php echo APP_ASSETS;?>img/common/other/bg_service_sp.jpg"></span>
    </div>
    <div class="cmn_service__inner">
      <ul class="cmn_service__list">
        <li>
          <div class="content">
            <p class="title item01">コスト削減サービス</p>
            <p class="desc">中小企業では営業会社による提案、お知り合い、友人関係での情報となるため偏りがあります。正しい相場、適正な情報を弊社が開示することで大企業並みのボリュームメリットによる削減が可能となります。</p>
            <a class="cmn_btn btn_border" href="<?php echo APP_URL;?>service/cost/"><span>サービス詳細</span></a>
          </div>
        </li>
        <li>
          <div class="content">
            <p class="title item02">営業動画作成サービス</p>
            <p class="desc">営業動画作成サービスは、自社のサービス内容をイメージと言葉で毎回、一言一句間違えずに相手に伝えることができます。<br>誰が見ても同じ理解が得られるので営業の再現性を向上に繋がります。</p>
            <a class="cmn_btn btn_border" href="<?php echo APP_URL;?>service/production/"><span>サービス詳細</span></a>
          </div>
        </li>
        <li>
          <div class="content">
            <p class="title item03">MEOサービス</p>
            <p class="desc">MEOとはGoogleマップから直接集客ができる仕組みです。エルネストリンクでは、コスト削減とMEOサービスでの集客を同時に行うことで他ではできない実質無料でのご提案を実現可能にしました。</p>
            <a class="cmn_btn btn_border" href="<?php echo APP_URL;?>service/meo/"><span>サービス詳細</span></a>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>
