<header id="header" class="header">
  <div class="header__main">
    <a class="header__main--logo" href="<?php echo APP_URL;?>"><img src="<?php echo APP_ASSETS;?>img/common/header/logo.svg" alt="Ernesto Link"></a>
    <div class="header__main--right">
      <ul class="header__main--menu">
        <li><a <?php if (isset($thisPageName) && $thisPageName == 'top') echo 'class="active"';?> href="<?php echo APP_URL;?>"><span>エルネストリンクTOP</span></a></li>
        <li><a <?php if (isset($thisPageName) && $thisPageName == 'philosophy') echo 'class="active"';?> href="<?php echo APP_URL;?>philosophy/"><span>企業理念</span></a></li>
        <li><a <?php if (isset($thisPageName) && ($thisPageName == 'case' || $thisPageName == 'case_detail')) echo 'class="active"';?> href="<?php echo APP_URL;?>case/"><span>導入事例</span></a></li>
        <li><a <?php if (isset($thisPageName) && $thisPageName == 'company') echo 'class="active"';?> href="<?php echo APP_URL;?>company/"><span>会社紹介</span></a></li>
      </ul>
      <ul class="header__main--btns">
        <li><a class="btn_service" href="javascript:;"><small>Service</small><span>サービス一覧</span></a></li>
        <li><a class="btn_contact" href="<?php echo APP_URL;?>contact/"><small>Contact</small><span>お問い合わせ</span></a></li>
        <li><a class="btn_consultation" href="https://www.chatwork.com/ernesto-link"><span>チャットによる<br>無料個別相談</span></a></li>
      </ul>
    </div>
    <a id="menu_open" class="header__main--menusp" href="javascript:;">
      <div class="lines">
        <span></span>
        <span></span>
        <span></span>
      </div>
    </a>
  </div>
  <div class="header__main--dropdown">
    <a class="btn_close" href="javascript:;">Close</a>
    <ul>
      <li>
        <a href="<?php echo APP_URL;?>service/cost/">
          <img class="lazy" src="<?php echo createSVG(250,150);?>" data-src="<?php echo APP_ASSETS;?>img/common/header/img_sub01.jpg" alt="コスト削減サービス">
          <p>コスト削減サービス</p>
        </a>
      </li>
      <li>
        <a href="<?php echo APP_URL;?>service/production/">
          <img class="lazy" src="<?php echo createSVG(250,150);?>" data-src="<?php echo APP_ASSETS;?>img/common/header/img_sub02.jpg" alt="営業動画作成サービス">
          <p>営業動画作成サービス</p>
        </a>
      </li>
      <li>
        <a href="<?php echo APP_URL;?>service/meo/">
          <img class="lazy" src="<?php echo createSVG(250,150);?>" data-src="<?php echo APP_ASSETS;?>img/common/header/img_sub03.jpg" alt="MEOサービス">
          <p>MEOサービス</p>
        </a>
      </li>
    </ul>
  </div>
  <div id="menu_sp" class="header__menusp">
    <div class="header__menusp--inner">
      <ul class="header__menusp--list">
        <li><a href="<?php echo APP_URL;?>"><span>エルネストリンクTOP</span></a></li>
        <li><a href="<?php echo APP_URL;?>philosophy/"><span>企業理念</span></a></li>
        <li><a href="<?php echo APP_URL;?>case/"><span>導入事例</span></a></li>
        <li><a href="<?php echo APP_URL;?>company/"><span>会社紹介</span></a></li>
        <li><a class="btn_blue btn_service has_sub" href="javascript:;"><span>サービス一覧</span></a>
          <ul class="sub">
            <li><a href="<?php echo APP_URL;?>service/cost/"><span>コスト削減サービス</span></a></li>
            <li><a href="<?php echo APP_URL;?>service/production/"><span>営業動画作成サービス</span></a></li>
            <li><a href="<?php echo APP_URL;?>service/meo/"><span>MEOサービス</span></a></li>
          </ul>
        </li>
        <li><a class="btn_blue btn_contact" href="<?php echo APP_URL;?>contact/"><span>お問い合わせ</span></a></li>
        <li><a class="btn_blue btn_consultation" href="https://www.chatwork.com/ernesto-link/"><span>チャットによる無料個別相談</span></a></li>
      </ul>
    </div>
  </div>
</header>
