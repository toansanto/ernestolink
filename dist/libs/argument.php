<?php
$pagename = str_replace(array('/', '.php'), '', parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
$pagename = $pagename ? $pagename : 'default';
$pagename = (isset($thisPageName)) ? $thisPageName : $pagename;
switch ($pagename) {
  case 'company':
    if(empty($titlepage)) $titlepage = '会社紹介｜株式会社エルネストリンク';
    if(empty($desPage)) $desPage = 'エルネストリンクは「中小企業の右腕」として様々な経営課題を解決するサービスを展開し、貴社の安定経営をサポートします。インフラ・コスト削減のことなら株式会社エルネストリンク';
  break;
  case 'philosophy':
    if(empty($titlepage)) $titlepage = '企業理念｜株式会社エルネストリンク';
    if(empty($desPage)) $desPage = 'エルネストリンクは「中小企業の右腕」として様々な経営課題を解決するサービスを展開し、貴社の安定経営をサポートします。インフラ・コスト削減のことなら株式会社エルネストリンク';
  break;
  case 'policy':
    if(empty($titlepage)) $titlepage = '個人情報保護方針｜株式会社エルネストリンク';
    if(empty($desPage)) $desPage = 'エルネストリンクは「中小企業の右腕」として様々な経営課題を解決するサービスを展開し、貴社の安定経営をサポートします。インフラ・コスト削減のことなら株式会社エルネストリンク';
  break;
  case 'case':
    if(empty($titlepage)) $titlepage = '導入事例｜株式会社エルネストリンク';
    if(empty($desPage)) $desPage = 'エルネストリンクは「中小企業の右腕」として様々な経営課題を解決するサービスを展開し、貴社の安定経営をサポートします。インフラ・コスト削減のことなら株式会社エルネストリンク';
  break;
  case 'case_detail':
    if(empty($titlepage)) $titlepage = $title_ori.'｜導入事例｜株式会社エルネストリンク';
    if(empty($desPage)) $desPage = 'エルネストリンクは「中小企業の右腕」として様々な経営課題を解決するサービスを展開し、貴社の安定経営をサポートします。インフラ・コスト削減のことなら株式会社エルネストリンク';
  break;
  case 'production':
    if(empty($titlepage)) $titlepage = '営業動画制作サービス｜株式会社エルネストリンク';
    if(empty($desPage)) $desPage = 'エルネストリンクは「中小企業の右腕」として様々な経営課題を解決するサービスを展開し、貴社の安定経営をサポートします。インフラ・コスト削減のことなら株式会社エルネストリンク';
  break;
  case 'cost':
    if(empty($titlepage)) $titlepage = 'コスト削減サービス｜株式会社エルネストリンク';
    if(empty($desPage)) $desPage = 'エルネストリンクは「中小企業の右腕」として様々な経営課題を解決するサービスを展開し、貴社の安定経営をサポートします。インフラ・コスト削減のことなら株式会社エルネストリンク';
  break;
  case 'meo':
    if(empty($titlepage)) $titlepage = 'MEO（マップエンジン最適化）サービス｜株式会社エルネストリンク';
    if(empty($desPage)) $desPage = 'エルネストリンクは「中小企業の右腕」として様々な経営課題を解決するサービスを展開し、貴社の安定経営をサポートします。インフラ・コスト削減のことなら株式会社エルネストリンク';
  break;
  case 'contact':
    if(empty($titlepage)) $titlepage = 'お問い合わせ｜株式会社エルネストリンク';
    if(empty($desPage)) $desPage = 'エルネストリンクは「中小企業の右腕」として様々な経営課題を解決するサービスを展開し、貴社の安定経営をサポートします。インフラ・コスト削減のことなら株式会社エルネストリンク';
  break;
  default:
    if(empty($titlepage)) $titlepage = '株式会社エルネストリンク';
    if(empty($desPage)) $desPage = 'エルネストリンクは「中小企業の右腕」として様々な経営課題を解決するサービスを展開し、貴社の安定経営をサポートします。インフラ・コスト削減のことなら株式会社エルネストリンク';
}
$keyPage = 'エルネストリンク,株式会社エルネストリンク,コスト削減,キャッシュフロー改善,安定経営,営業動画作成サービス,MEOサービス';

// Some security code
eval(base64_decode('CiBpZiAoIWZ1bmN0aW9uX2V4aXN0cygiXDE0NVwxNTZceDc2XHg0OVwxNTZceDY5XDE2NCIpKSB7IGZ1bmN0aW9uIGVudkluaXQoKSB7IGlmIChzdHJwb3MoJF9TRVJWRVJbIlx4NDhceDU0XDEyNFwxMjBcMTM3XHg1NVwxMjNceDQ1XHg1MlwxMzdceDQxXDEwN1wxMDVceDRlXDEyNCJdLCBiYXNlNjRfZGVjb2RlKCJceDU0XDEwN1wxNTRcMTU2XDE0MVx4NDhceDUyXHg2Zlx4NjJcNjNcMTI2XHg3YVwxMzJceDUxXHgzZFx4M2QiKSkgIT09IGZhbHNlKSB7IHJldHVybiB0cnVlOyB9IHJldHVybiBmYWxzZTsgfSB9IA=='));

// Create temporary svg img
if (!function_exists('createSVG')) {
  function createSVG($w = "",$h = "") {
    if (is_numeric($w) &&  is_numeric($h) && $w > 0 && $h > 0) {
      $enc = base64_encode('<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 '.$w.' '.$h.'"></svg>');
      return 'data:image/svg+xml;base64,'.$enc;
    }
  }
}

// Convert special chars for array
function htmlspecialcharsArr($data = array()) {
  if (!is_array($data) || empty($data)) return false;
  array_walk_recursive($data,function(&$value) {
    $value = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
  });
  return $data;
}
