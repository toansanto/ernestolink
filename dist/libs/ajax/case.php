<?php
if (
  isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
  strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' &&
  isset($_POST['post_type']) &&
  !empty($_POST['post_type']) &&
  isset($_POST['page']) &&
  !empty($_POST['page']) &&
  isset($_POST['posts_per_page']) &&
  !empty($_POST['posts_per_page'])
) {
  include_once('../../cms/wp-load.php');

  // Init param
  $param = $_POST;

  // Setup tax query
  $tax_query = "";
  if (isset($param['taxonomy']) && !empty($param['taxonomy'])) {
    $tax_query = array(
      array(
        'taxonomy' => 'casecat',
        'field' => 'term_id',
        'terms' => explode(",",$param['taxonomy'])
      )
    );
  }

  // Main query
  $case = new WP_Query(array(
    'post_type' => $param['post_type'],
    'posts_per_page' => $param['posts_per_page'],
    'paged' => $param['page'],
    'tax_query' => $tax_query,
  ));
  if ($case->have_posts()) : while ($case->have_posts()) : $case->the_post();
  $terms = wp_get_post_terms($post->ID,'casecat',array('fields'=>'names'));
  $thumb = (get_the_post_thumbnail_url()) ? get_the_post_thumbnail_url() : APP_ASSETS.'img/common/other/img_nophoto.jpg';
  $chk_new = checkNew($post->ID);
?>
<li class="case_archive__list--item <?php if ($param['page'] == $case->max_num_pages) echo 'lastpage';?>">
  <a href="<?php echo the_permalink();?>">
    <div class="content">
      <?php if ($chk_new == true) { ?><span class="new">New</span><?php } ?>
      <?php if ($terms) { ?><span class="cat"><?php echo $terms[0];?></span><?php } ?>
      <div class="img">
        <span class="thumb lazy" data-bg="<?php echo $thumb;?>"></span>
      </div>
      <div class="txt">
        <h3 class="title"><?php echo $post->post_title;?>様</h3>
        <p class="desc"><?php echo makeDesc($post->post_content);?></p>
      </div>
    </div>
  </a>
</li>
<?php endwhile; endif; wp_reset_postdata(); } ?>
