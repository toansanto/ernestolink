<?php
session_start();
header("Cache-control: private");
header_remove("Expires");
header_remove("Cache-Control");
header_remove("Pragma");
header_remove("Last-Modified");
ob_start();
if (!isset($_SESSION['unique'])) {
	$_SESSION['unique'] = time();
}
include_once(dirname(__DIR__) . '/app_config.php');
$thisPageName = 'contact';
$type = (isset($_GET['opt']) && $_GET['opt'] != '') ? $_GET['opt'] : '';
include(APP_PATH.'libs/head.php');
?>
<meta http-equiv="expires" content="86400">
<meta name="format-detection" content="telephone=no">
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/contact.min.css">
</head>
<body id="contact" class="contact">
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
  <main>
    <div class="cmn_hd">
      <div class="cmn_hd__inner">
        <div class="cmn_hd__bg">
          <span class="thumb lazy pc" data-bg="<?php echo APP_ASSETS;?>img/contact/img_main.jpg"></span>
          <span class="thumb lazy sp" data-bg="<?php echo APP_ASSETS;?>img/contact/img_main_sp.jpg"></span>
        </div>
        <div class="wcm cmn_hd__title">
          <span class="cmn_hd__title--en">Contact</span>
          <h1 class="cmn_hd__title--jp">お問い合わせ</h1>
        </div>
      </div>
    </div>
    <div class="breadcrumbs">
      <ul class="wcm">
        <li><a href="<?php echo APP_URL;?>">TOP</a></li>
        <li><span>お問い合わせ</span></li>
      </ul>
    </div>
    <div class="wcm contact__inner">
      <ul class="contact__methods">
        <li><a class="btn_contact" href="#contactform"><span>メールでのお問い合わせ</span></a></li>
        <li><a class="btn_chatwork" href="https://www.chatwork.com/ernesto-link"><img class="lazy" src="<?php echo createSVG(118,24);?>" data-src="<?php echo APP_ASSETS;?>img/common/icon/ico_chatwork.svg" alt="Chatwork"><span>でのお問い合わせ</span></a></li>
        <li><a class="btn_zoom" href="<?php echo APP_URL;?>contact/"><img class="lazy" src="<?php echo createSVG(81,18);?>" data-src="<?php echo APP_ASSETS;?>img/common/icon/ico_zoom.svg" alt="Zoom"><span>でのお問い合わせ</span></a></li>
      </ul>
      <form id="contactform" method="post" class="contact__form" action="<?php echo './confirm/?g=' . time() ?>" name="contactform" onSubmit="return check()" enctype="multipart/form-data">
        <p class="hid_url">Leave this empty: <input type="text" name="url"></p><!-- Anti spam part1: the contact form -->
        <div class="contact__form--part">
          <h3 class="part_title">お客様情報</h3>
          <div class="part_list">
            <div class="part_list__item mb">
              <div class="part_list__item--lb">お問い合わせ項目<em>【必須】</em></div>
              <div class="part_list__item--ct">
                <ul class="bl_checkbox">
                  <li><label><input <?php if ($type == 1) echo 'checked';?> name="method" type="radio" value="コスト削減のご相談" class="validate[required]"><span>コスト削減のご相談</span></label></li>
                  <li><label><input <?php if ($type == 2) echo 'checked';?> name="method" type="radio" value="資料請求のご依頼" class="validate[required]"><span>資料請求のご依頼</span></label></li>
                  <li><label><input <?php if ($type == 3) echo 'checked';?> name="method" type="radio" value="その他のお問い合わせ" class="validate[required]"><span>その他のお問い合わせ</span></label></li>
                </ul>
              </div>
            </div>
						<div class="part_list__item">
              <div class="part_list__item--lb">ご連絡・ご相談方法<em>【必須】</em></div>
              <div class="part_list__item--ct">
                <ul class="bl_checkbox">
                  <li><label><input name="type" type="radio" value="電話" class="validate[required]"><span>電話</span></label></li>
                  <li><label><input name="type" type="radio" value="メール" class="validate[required]"><span>メール</span></label></li>
                  <li><label><input name="type" type="radio" value="Chatwork" class="validate[required]"><span>Chatwork</span></label></li>
									<li><label><input name="type" type="radio" value="Zoom" class="validate[required]"><span>Zoom</span></label></li>
                </ul>
              </div>
            </div>
            <div class="part_list__item">
              <div class="part_list__item--lb">ご担当者<em>【必須】</em></div>
              <div class="part_list__item--ct">
                <input type="text" name="nameuser" id="nameuser" class="validate[required]" placeholder="例  谷岡 遼">
              </div>
            </div>
            <div class="part_list__item">
              <div class="part_list__item--lb">ふりがな<em>【必須】</em></div>
              <div class="part_list__item--ct">
                <input type="text" name="furigana" id="furigana" class="validate[required,custom[furigana]]" placeholder="例  たにおか りょう">
              </div>
            </div>
            <div class="part_list__item">
              <div class="part_list__item--lb">会社名<em>【必須】</em></div>
              <div class="part_list__item--ct">
                <input type="text" name="company" id="company" class="validate[required]" placeholder="株式会社エルネストリンク">
              </div>
            </div>
            <div class="part_list__item">
              <div class="part_list__item--lb">メールアドレス<em>【必須】</em></div>
              <div class="part_list__item--ct">
                <input type="email" name="email" id="email" class="validate[required,custom[email]]">
              </div>
            </div>
            <div class="part_list__item">
              <div class="part_list__item--lb">郵便番号</div>
              <div class="part_list__item--ct">
                <div class="bl_zipcode">
                  <input type="tel" name="zipcode01" id="zipcode01" class="validate[maxSize[3]">
                  <span class="dash">ー</span>
                  <input type="tel" name="zipcode02" id="zipcode02" class="validate[maxSize[4]">
                </div>
              </div>
            </div>
            <div class="part_list__item">
              <div class="part_list__item--lb">住所<em>【必須】</em></div>
              <div class="part_list__item--ct">
                <input type="text" name="address" id="address" class="validate[required]">
              </div>
            </div>
            <div class="part_list__item">
              <div class="part_list__item--lb">電話番号<em>【必須】</em></div>
              <div class="part_list__item--ct">
                <input type="tel" name="tel" id="tel" class="validate[required,custom[phone]]" placeholder="ハイフンを入れて入力ください。">
              </div>
            </div>
            <div class="part_list__item">
              <div class="part_list__item--lb">ご紹介者</div>
              <div class="part_list__item--ct">
                <input type="text" name="introducer" id="introducer" placeholder="例 株式会社ABC 佐藤太郎">
              </div>
            </div>
            <div class="part_list__item v-top">
              <div class="part_list__item--lb">お問い合わせ内容<em>【必須】</em></div>
              <div class="part_list__item--ct">
                <textarea name="content" id="content" class="validate[required]"></textarea>
              </div>
            </div>
          </div>
					<div class="box_rules">
	          <div class="box_rules__inner">
	            <p class="box_rules__title">個人情報の取り扱いについて</p>
	            <div class="box_rules__content">
	              <p>お客様よりご登録いただいた個人情報は次の目的にのみ使用します<br>
	              ・弊社から事務的な連絡事項を通知するため<br>
	              ・サービス紹介など、お客様にとって有益と弊社が判断する情報をご案内するため</p>
	              <a href="<?php echo APP_URL;?>policy/">※個人情報保護方針はこちら ></a>
	            </div>
	            <div class="chk_confirm">
	              <label><input type="checkbox" name="check1" value="ok"><span>個人情報の取扱いについて同意の上送信します</span></label>
	            </div>
	          </div>
	        </div>
	        <button id="btnConfirm" class="btn_submit"><span>入力内容を確認する</span><i></i></button>
	        <input type="hidden" name="actionFlag" value="confirm">
          <p class="part_note">経費削減試算無料申込みをご希望の方は<br class="sp">下記項目もご入力ください</p>
        </div>
        <div class="contact__form--part">
          <h3 class="part_title">経費削減試算無料申込み</h3>
          <p class="part_desc">試算希望項目の必要書類を<br class="sp">下記に添付お願い致します</p>
          <div class="part_list part_files">
            <div class="part_list__item">
              <div class="part_list__item--lb">複合機必要書類</div>
              <div class="part_list__item--ct">
                <ul class="bl_files">
                  <li>
                    <p class="lb">リース契約書</p>
                    <label for="file_01"><input type="file" id="file_01" name="file_01">
                      <span class="btn">ファイルを選択</span>
                      <span class="note">選択されていません</span>
                    </label>
                  </li>
                  <li>
                    <p class="lb">リース支払い明細</p>
                    <label for="file_02"><input type="file" id="file_02" name="file_02">
                      <span class="btn">ファイルを選択</span>
                      <span class="note">選択されていません</span>
                    </label>
                  </li>
                  <li>
                    <p class="lb">保守契約書</p>
                    <label for="file_03"><input type="file" id="file_03" name="file_03">
                      <span class="btn">ファイルを選択</span>
                      <span class="note">選択されていません</span>
                    </label>
                  </li>
                  <li>
                    <p class="lb">カウンター料金のご利用明細（3ヶ月分）</p>
                    <label for="file_04"><input type="file" id="file_04" name="file_04">
                      <span class="btn">ファイルを選択</span>
                      <span class="note">選択されていません</span>
                    </label>
                  </li>
                </ul>
              </div>
            </div>
            <div class="part_list__item">
              <div class="part_list__item--lb">固定電話必要書類</div>
              <div class="part_list__item--ct">
                <ul class="bl_files">
                  <li>
                    <p class="lb">ご利用明細書（2ヶ月分）</p>
                    <label for="file_05"><input type="file" id="file_05" name="file_05">
                      <span class="btn">ファイルを選択</span>
                      <span class="note">選択されていません</span>
                    </label>
                  </li>
                </ul>
              </div>
            </div>
            <div class="part_list__item">
              <div class="part_list__item--lb">電気必要書類</div>
              <div class="part_list__item--ct">
                <ul class="bl_files">
                  <li>
                    <p class="lb">検針票（1年分）</p>
                    <label for="file_06"><input type="file" id="file_06" name="file_06">
                      <span class="btn">ファイルを選択</span>
                      <span class="note">選択されていません</span>
                    </label>
                  </li>
                  <li>
                    <p class="lb">業種</p>
                    <input type="text" name="industry" id="industry">
                  </li>
                  <li class="cols2">
                    <div class="subrow">
                      <p class="lb">営業時間</p>
                      <input type="text" name="business_hours" id="business_hours">
                    </div>
                    <div class="subrow">
                      <p class="lb">休日</p>
                      <input type="text" name="holiday" id="holiday">
                    </div>
                  </li>
                  <li>
                    <p class="lb">営業時間以外に稼働している設備はありますか？</p>
                    <input type="text" name="question01" id="question01">
                  </li>
                  <li>
                    <p class="lb">御社の繁忙期は何月ですか？</p>
                    <input type="text" name="question02" id="question02">
                  </li>
                  <li>
                    <p class="lb">電気料金が１番高いのは何月ですか？</p>
                    <input type="text" name="question03" id="question03">
                  </li>
                </ul>
              </div>
            </div>
            <div class="part_list__item">
              <div class="part_list__item--lb">携帯電話必要書類</div>
              <div class="part_list__item--ct">
                <ul class="bl_files">
                  <li>
                    <p class="lb">ご利用明細書（3ヶ月分）</p>
                    <label for="file_07"><input type="file" id="file_07" name="file_07">
                      <span class="btn">ファイルを選択</span>
                      <span class="note">選択されていません</span>
                    </label>
                  </li>
                </ul>
              </div>
            </div>
            <div class="part_list__item">
              <div class="part_list__item--lb">インターネット必要書類</div>
              <div class="part_list__item--ct">
                <ul class="bl_files">
                  <li>
                    <p class="lb">ご利用明細書（1ヶ月分）</p>
                    <label for="file_08"><input type="file" id="file_08" name="file_08">
                      <span class="btn">ファイルを選択</span>
                      <span class="note">選択されていません</span>
                    </label>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
				<?php /*
        <button id="btnConfirm_bot" class="btn_submit"><span>入力内容を確認する</span><i></i></button>
				*/ ?>
      </form>
    </div>
  </main>
</div>
<?php include(APP_PATH.'libs/footer.php'); ?>
<script src="<?php echo APP_ASSETS; ?>js/form/ajaxzip3.js"></script>
<script src="<?php echo APP_ASSETS; ?>js/form/jquery.validationEngine.js"></script>
<script src="<?php echo APP_ASSETS; ?>js/form/languages/jquery.validationEngine-ja.js"></script>
<script src="<?php echo APP_ASSETS; ?>js/page/contact.min.js"></script>
<?php if ($type != '') { ?>
<script>
$(window).load(function() {
	$('html,body').animate({scrollTop:$('#contactform').offset().top - getHeaderHeight()},500);
})
</script>
<?php } ?>
</body>
</html>
