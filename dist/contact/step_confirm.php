<?php
session_start();
ob_start();
error_reporting(0);
$thisPageName = 'contact';
include_once(dirname(__DIR__) . '/app_config.php');
include_once(APP_PATH.'libs/head.php');
if(empty($_POST['actionFlag']) && empty($_SESSION['statusFlag'])) header('location: '.APP_URL);

$gtime = time();

//always keep this
$actionFlag          = (!empty($_POST['actionFlag'])) ? htmlspecialchars($_POST['actionFlag']) : '';
$reg_url             = (!empty($_POST['url'])) ? htmlspecialchars($_POST['url']) : '';
//end always keep this

//お問い合わせフォーム内容
$reg_method          = (!empty($_POST['method'])) ? htmlspecialchars($_POST['method']) : '';
$reg_type            = (!empty($_POST['type'])) ? htmlspecialchars($_POST['type']) : '';
$reg_name            = (!empty($_POST['nameuser'])) ? htmlspecialchars($_POST['nameuser']) : '';
$reg_furigana        = (!empty($_POST['furigana'])) ? htmlspecialchars($_POST['furigana']) : '';
$reg_company         = (!empty($_POST['company'])) ? htmlspecialchars($_POST['company']) : '';
$reg_zipcode01       = (!empty($_POST['zipcode01'])) ? htmlspecialchars($_POST['zipcode01']) : '';
$reg_zipcode02       = (!empty($_POST['zipcode02'])) ? htmlspecialchars($_POST['zipcode02']) : '';
$reg_address         = (!empty($_POST['address'])) ? htmlspecialchars($_POST['address']) : '';
$reg_tel             = (!empty($_POST['tel'])) ? htmlspecialchars($_POST['tel']) : '';
$reg_email           = (!empty($_POST['email'])) ? htmlspecialchars($_POST['email']) : '';
$reg_introducer      = (!empty($_POST['introducer'])) ? htmlspecialchars($_POST['introducer']) : '';
$reg_content         = (!empty($_POST['content'])) ? htmlspecialchars($_POST['content']) : '';
$br_reg_content      = nl2br($reg_content);

// Sub parts
$reg_industry        = (!empty($_POST['industry'])) ? htmlspecialchars($_POST['industry']) : '';
$reg_business_hours  = (!empty($_POST['business_hours'])) ? htmlspecialchars($_POST['business_hours']) : '';
$reg_holiday         = (!empty($_POST['holiday'])) ? htmlspecialchars($_POST['holiday']) : '';
$reg_question01      = (!empty($_POST['question01'])) ? htmlspecialchars($_POST['question01']) : '';
$reg_question02      = (!empty($_POST['question02'])) ? htmlspecialchars($_POST['question02']) : '';
$reg_question03      = (!empty($_POST['question03'])) ? htmlspecialchars($_POST['question03']) : '';

// List image
$reg_listimg         = (!empty($_POST['listimg'])) ? htmlspecialcharsArr($_POST['listimg']) : array();

// File upload
$extensions= array("jpeg","jpg","png","pdf");
$listImg = array();

for ($i = 1; $i <= 8; $i++) {
  if(isset($_FILES['file_0'.$i]) && $_FILES['file_0'.$i]['size'] > 0){
    ${"errors0".$i} = array();
    ${"file_name0".$i} = $_SESSION['unique'].'_'.$_FILES['file_0'.$i]['name'];
    ${"file_size0".$i} = $_FILES['file_0'.$i]['size'];
    ${"file_tmp0".$i} = $_FILES['file_0'.$i]['tmp_name'];
    ${"file_ext0".$i} = strtolower(end(explode('.',$_FILES['file_0'.$i]['name'])));

    if(in_array(${"file_ext0".$i},$extensions)=== false && $_FILES['file_0'.$i]['name'] != ''){
       ${"errors0".$i}[]="Extension not allowed, please choose a JPEG or PNG or PDF file.";
    }

    if(${"file_size0".$i} > 3145728){
       ${"errors0".$i}[]='File size must be less than 5 MB.';
    }

    if(empty(${"errors0".$i})==true){
      move_uploaded_file(${"file_tmp0".$i},"uploads/".${"file_name0".$i});
      ${"getfile0".$i} = 'uploads/'.${"file_name0".$i};
      $listImg['file_0'.$i] = array(
       'url' => APP_URL.'contact/'.${"getfile0".$i},
       'name' => $_FILES['file_0'.$i]['name'],
       'realname' => ${"file_name0".$i},
      );
    } else {
      ${"geterrors0".$i} = "Extension not allowed or File size is too large.";
    }
  }
}

if($actionFlag == "confirm") {
  $_SESSION['ses_from_step2'] = true;
  if(!isset($_SESSION['ses_gtime_step2'])) $_SESSION['ses_gtime_step2'] = $gtime;
?>
<meta name="format-detection" content="telephone=no">
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/contact.min.css">
</head>
<body id="contact" class="cmn_form confirm_page contact no_fixed_hd">
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
  <main>
    <div class="cmn_hd">
      <div class="cmn_hd__inner">
        <div class="cmn_hd__bg">
          <span class="thumb lazy pc" data-bg="<?php echo APP_ASSETS;?>img/contact/img_main.jpg"></span>
          <span class="thumb lazy sp" data-bg="<?php echo APP_ASSETS;?>img/contact/img_main_sp.jpg"></span>
        </div>
        <div class="wcm cmn_hd__title">
          <span class="cmn_hd__title--en">Contact</span>
          <h1 class="cmn_hd__title--jp">お問い合わせ</h1>
        </div>
      </div>
    </div>
    <div class="wcm contact__inner">
      <form method="post" class="contact__form confirmform" action="../complete/?g=<?php echo $gtime ?>" name="confirmform" id="confirmform">
        <p class="hid_url">Leave this empty: <input type="text" name="url" value="<?php echo $reg_url ?>"></p><!-- Anti spam part1: the contact form -->
        <div class="contact__form--part">
          <h3 class="part_title">お客様情報</h3>
          <div class="part_list">
            <div class="part_list__item">
              <div class="part_list__item--lb">お問い合わせ項目</div>
              <div class="part_list__item--ct"><?php echo $reg_method;?></div>
            </div>
            <div class="part_list__item">
              <div class="part_list__item--lb">ご連絡・ご相談方法</div>
              <div class="part_list__item--ct"><?php echo $reg_type;?></div>
            </div>
            <div class="part_list__item">
              <div class="part_list__item--lb">ご担当者</div>
              <div class="part_list__item--ct"><?php echo $reg_name;?></div>
            </div>
            <div class="part_list__item">
              <div class="part_list__item--lb">ふりがな</div>
              <div class="part_list__item--ct"><?php echo $reg_furigana;?></div>
            </div>
            <div class="part_list__item">
              <div class="part_list__item--lb">会社名</div>
              <div class="part_list__item--ct"><?php echo $reg_company;?></div>
            </div>
            <div class="part_list__item">
              <div class="part_list__item--lb">メールアドレス</div>
              <div class="part_list__item--ct"><?php echo $reg_email;?></div>
            </div>
            <?php if ($reg_zipcode01 != '' && $reg_zipcode02 != '') { ?>
            <div class="part_list__item">
              <div class="part_list__item--lb">郵便番号</div>
              <div class="part_list__item--ct"><?php echo $reg_zipcode01."-".$reg_zipcode02;?></div>
            </div>
            <?php } ?>
            <div class="part_list__item">
              <div class="part_list__item--lb">住所</div>
              <div class="part_list__item--ct"><?php echo $reg_address;?></div>
            </div>
            <div class="part_list__item">
              <div class="part_list__item--lb">電話番号</div>
              <div class="part_list__item--ct"><?php echo $reg_tel;?></div>
            </div>
            <?php if ($reg_introducer != '') { ?>
            <div class="part_list__item">
              <div class="part_list__item--lb">ご紹介者</div>
              <div class="part_list__item--ct"><?php echo $reg_introducer;?></div>
            </div>
            <?php } ?>
            <div class="part_list__item v-top">
              <div class="part_list__item--lb">お問い合わせ内容</div>
              <div class="part_list__item--ct"><?php echo $br_reg_content;?></div>
            </div>
          </div>
        </div>
        <?php if (
          !empty($listImg['file_01']) ||
          !empty($listImg['file_02']) ||
          !empty($listImg['file_03']) ||
          !empty($listImg['file_04']) ||
          !empty($listImg['file_05']) ||
          !empty($listImg['file_06']) ||
          !empty($listImg['file_07']) ||
          !empty($listImg['file_08']) ||
          $reg_industry != '' ||
          $reg_business_hours != '' ||
          $reg_holiday != '' ||
          $reg_question01 != '' ||
          $reg_question02 != '' ||
          $reg_question03 != ''
        ) { ?>
        <div class="contact__form--part">
          <h3 class="part_title">経費削減試算無料申込み</h3>
          <div class="part_list">
            <?php if (
              !empty($listImg['file_01']) ||
              !empty($listImg['file_02']) ||
              !empty($listImg['file_03']) ||
              !empty($listImg['file_04'])
            ) { ?>
            <div class="part_list__item v-top">
              <div class="part_list__item--lb">複合機必要書類</div>
              <div class="part_list__item--ct">
                <ul class="bl_result">
                  <?php if (!empty($listImg['file_01'])) { ?>
                  <li>
                    <p>リース契約書</p>
                    <?php echo $listImg['file_01']['name'];?>
                  </li>
                  <?php } ?>
                  <?php if (!empty($listImg['file_02'])) { ?>
                  <li>
                    <p>リース支払い明細</p>
                    <?php echo $listImg['file_02']['name'];?>
                  </li>
                  <?php } ?>
                  <?php if (!empty($listImg['file_03'])) { ?>
                  <li>
                    <p>保守契約書</p>
                    <?php echo $listImg['file_03']['name'];?>
                  </li>
                  <?php } ?>
                  <?php if (!empty($listImg['file_04'])) { ?>
                  <li>
                    <p>カウンター料金のご利用明細（3ヶ月分）</p>
                    <?php echo $listImg['file_04']['name'];?>
                  </li>
                  <?php } ?>
                </ul>
              </div>
            </div>
            <?php } ?>
            <?php if (!empty($listImg['file_05'])) { ?>
            <div class="part_list__item v-top">
              <div class="part_list__item--lb">固定電話必要書類</div>
              <div class="part_list__item--ct">
                <ul class="bl_result">
                  <li>
                    <p>ご利用明細書（2ヶ月分）</p>
                    <?php echo $listImg['file_05']['name'];?>
                  </li>
                </ul>
              </div>
            </div>
            <?php } ?>
            <?php if (
              !empty($listImg['file_06']) ||
              $reg_industry != '' ||
              $reg_business_hours != '' ||
              $reg_holiday != '' ||
              $reg_question01 != '' ||
              $reg_question02 != '' ||
              $reg_question03 != ''
            ) { ?>
            <div class="part_list__item v-top">
              <div class="part_list__item--lb">電気必要書類</div>
              <div class="part_list__item--ct">
                <ul class="bl_result">
                  <?php if (!empty($listImg['file_06'])) { ?>
                  <li>
                    <p>検針票（1年分）</p>
                    <?php echo $listImg['file_06']['name'];?>
                  </li>
                  <?php } ?>
                  <?php if ($reg_industry != '') { ?>
                  <li>
                    <p>業種</p>
                    <?php echo $reg_industry;?>
                  </li>
                  <?php } ?>
                  <?php if ($reg_business_hours != '') { ?>
                  <li>
                    <p>営業時間</p>
                    <?php echo $reg_business_hours;?>
                  </li>
                  <?php } ?>
                  <?php if ($reg_holiday != '') { ?>
                  <li>
                    <p>休日</p>
                    <?php echo $reg_holiday;?>
                  </li>
                  <?php } ?>
                  <?php if ($reg_question01 != '') { ?>
                  <li>
                    <p>営業時間以外に稼働している設備はありますか？</p>
                    <?php echo $reg_question01;?>
                  </li>
                  <?php } ?>
                  <?php if ($reg_question02 != '') { ?>
                  <li>
                    <p>御社の繁忙期は何月ですか？</p>
                    <?php echo $reg_question02;?>
                  </li>
                  <?php } ?>
                  <?php if ($reg_question03 != '') { ?>
                  <li>
                    <p>電気料金が１番高いのは何月ですか？</p>
                    <?php echo $reg_question03;?>
                  </li>
                  <?php } ?>
                </ul>
              </div>
            </div>
            <?php } ?>
            <?php if (!empty($listImg['file_07'])) { ?>
            <div class="part_list__item v-top">
              <div class="part_list__item--lb">携帯電話必要書類</div>
              <div class="part_list__item--ct">
                <ul class="bl_result">
                  <li>
                    <p>ご利用明細書（3ヶ月分）</p>
                    <?php echo $listImg['file_07']['name'];?>
                  </li>
                </ul>
              </div>
            </div>
            <?php } ?>
            <?php if (!empty($listImg['file_08'])) { ?>
            <div class="part_list__item v-top">
              <div class="part_list__item--lb">インターネット必要書類</div>
              <div class="part_list__item--ct">
                <ul class="bl_result">
                  <li>
                    <p>ご利用明細書（1ヶ月分）</p>
                    <?php echo $listImg['file_08']['name'];?>
                  </li>
                </ul>
              </div>
            </div>
            <?php } ?>
          </div>
        </div>
        <?php } ?>
        <input type="hidden" name="method" value="<?php echo $reg_method ?>">
        <input type="hidden" name="type" value="<?php echo $reg_type ?>">
        <input type="hidden" name="nameuser" value="<?php echo $reg_name ?>">
        <input type="hidden" name="furigana" value="<?php echo $reg_furigana ?>">
        <input type="hidden" name="company" value="<?php echo $reg_company ?>">
        <input type="hidden" name="zipcode01" value="<?php echo $reg_zipcode01 ?>">
        <input type="hidden" name="zipcode02" value="<?php echo $reg_zipcode02 ?>">
        <input type="hidden" name="address" value="<?php echo $reg_address ?>">
        <input type="hidden" name="tel" value="<?php echo $reg_tel ?>">
        <input type="hidden" name="email" value="<?php echo $reg_email ?>">
        <input type="hidden" name="content" value="<?php echo $reg_content ?>">
        <?php if ($listImg) {
          foreach ($listImg as $key => $item) {
            echo '<input type="hidden" name="listimg['.$key.'][realname]" value="'.$item['realname'].'" />';
            echo '<input type="hidden" name="listimg['.$key.'][name]" value="'.$item['name'].'" />';
          }
        } ?>
        <input type="hidden" name="industry" value="<?php echo $reg_industry ?>">
        <input type="hidden" name="business_hours" value="<?php echo $reg_business_hours ?>">
        <input type="hidden" name="holiday" value="<?php echo $reg_holiday ?>">
        <input type="hidden" name="question01" value="<?php echo $reg_question01 ?>">
        <input type="hidden" name="question02" value="<?php echo $reg_question02 ?>">
        <input type="hidden" name="question03" value="<?php echo $reg_question03 ?>">
        <!-- always keep this -->
        <input type="hidden" name="url" value="<?php echo $reg_url ?>">
        <!-- end always keep this -->

        <p class="btn_back">
          <a href="javascript:history.back()">入力内容を修正する</a>
        </p>
        <button id="btnSend" class="btn_submit"><span>上記内容で送信する</span></button>
        <input type="hidden" name="actionFlag" value="send">
      </form>
    </div>
  </main>
</div>
<?php include(APP_PATH.'libs/footer.php'); ?>
<script src="<?php echo APP_ASSETS;?>js/page/contact.min.js"></script>
</body>
</html>
<?php } ?>
