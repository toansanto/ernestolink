<?php
include_once(dirname(__FILE__) . '/step_confirm.php');
if($actionFlag == 'send') {
require(APP_PATH."libs/form/jphpmailer.php");
$aMailto = $aMailtoContact;
if(count($aBccToContact)) $aBccTo = $aBccToContact;
$from = $fromContact;
$fromname = $fromName;
$subject_admin = "ホームページからお問い合わせがありました";
$subject_user = "お問い合わせありがとうございました";
$email_head_ctm_admin = "ホームページからお問い合わせがありました。";
$email_head_ctm_user = "この度はお問い合わせいただきまして誠にありがとうございます。
こちらは自動返信メールとなっております。
弊社にて確認した後、改めてご連絡させていただきます。

以下、お問い合わせ内容となっております。
ご確認くださいませ。";
$email_body_footer = "
株式会社エルネストリンク
〒540-0029
大阪府大阪市中央区本町橋2-17 AXIS内本町ビル9F
TEL.06-6910-1334
";

$entry_time = gmdate("Y/m/d H:i:s",time()+9*3600);
$entry_host = gethostbyaddr(getenv("REMOTE_ADDR"));
$entry_ua = getenv("HTTP_USER_AGENT");

$msgBody = "
---------------------------------------------------------------
お客様情報
---------------------------------------------------------------

■お問い合わせ項目
$reg_method

■ご連絡・ご相談方法
$reg_type

■ご担当者
$reg_name

■ふりがな
$reg_furigana

■会社名
$reg_company

■メールアドレス
$reg_email
";

if (isset($reg_zipcode01) && $reg_zipcode01 != '' && isset($reg_zipcode02) && $reg_zipcode02 != '') $msgBody .= "
■郵便番号
{$reg_zipcode01}-{$reg_zipcode02}
";

$msgBody .= "
■住所
$reg_address

■電話番号
$reg_tel
";

if(isset($reg_introducer) && !empty($reg_introducer)) $msgBody .= "
■ご紹介者
$reg_introducer
";

$msgBody .= "
■お問い合わせ内容
$reg_content
";

if (
  !empty($listImg['file_01']) ||
  !empty($listImg['file_02']) ||
  !empty($listImg['file_03']) ||
  !empty($listImg['file_04']) ||
  !empty($listImg['file_05']) ||
  !empty($listImg['file_06']) ||
  !empty($listImg['file_07']) ||
  !empty($listImg['file_08']) ||
  $reg_industry != '' ||
  $reg_business_hours != '' ||
  $reg_holiday != '' ||
  $reg_question01 != '' ||
  $reg_question02 != '' ||
  $reg_question03 != ''
) $msgBody .= "

---------------------------------------------------------------
経費削減試算無料申込み
---------------------------------------------------------------
";

// Sub part 01
if (
  !empty($reg_listimg['file_01']) ||
  !empty($reg_listimg['file_02']) ||
  !empty($reg_listimg['file_03']) ||
  !empty($reg_listimg['file_04'])
) $msgBody .= "
■複合機必要書類
";
if(isset($reg_listimg['file_01']) && !empty($reg_listimg['file_01'])) $msgBody .= "リース契約書
{$reg_listimg['file_01']['name']}
";
if(isset($reg_listimg['file_02']) && !empty($reg_listimg['file_02'])) $msgBody .= "リース支払い明細
{$reg_listimg['file_02']['name']}
";
if(isset($reg_listimg['file_03']) && !empty($reg_listimg['file_03'])) $msgBody .= "保守契約書
{$reg_listimg['file_03']['name']}
";
if(isset($reg_listimg['file_04']) && !empty($reg_listimg['file_04'])) $msgBody .= "リース契約書
{$reg_listimg['file_04']['name']}
";

// Sub part 02
if (
  !empty($reg_listimg['file_05'])
) $msgBody .= "
■固定電話必要書類
ご利用明細書（2ヶ月分）
{$reg_listimg['file_05']['name']}
";

// Sub part 03
if (
  !empty($listImg['file_06']) ||
  $reg_industry != '' ||
  $reg_business_hours != '' ||
  $reg_holiday != '' ||
  $reg_question01 != '' ||
  $reg_question02 != '' ||
  $reg_question03 != ''
) $msgBody .= "
■電気必要書類
";
if(isset($reg_listimg['file_06']) && !empty($reg_listimg['file_06'])) $msgBody .= "検針票（1年分）
{$reg_listimg['file_06']['name']}
";
if(isset($reg_industry) && $reg_industry != '') $msgBody .= "業種
$reg_industry
";
if(isset($reg_business_hours) && $reg_business_hours != '') $msgBody .= "営業時間
$reg_business_hours
";
if(isset($reg_holiday) && $reg_holiday != '') $msgBody .= "休日
$reg_holiday
";
if(isset($reg_question01) && $reg_question01 != '') $msgBody .= "営業時間以外に稼働している設備はありますか？
$reg_question01
";
if(isset($reg_question02) && $reg_question02 != '') $msgBody .= "御社の繁忙期は何月ですか？
$reg_question02
";
if(isset($reg_question03) && $reg_question03 != '') $msgBody .= "電気料金が１番高いのは何月ですか？
$reg_question03
";

// Sub part 04
if (
  !empty($reg_listimg['file_07'])
) $msgBody .= "
■携帯電話必要書類
ご利用明細書（3ヶ月分）
{$reg_listimg['file_07']['name']}
";

// Sub part 05
if (
  !empty($reg_listimg['file_08'])
) $msgBody .= "
■インターネット必要書類
ご利用明細書（1ヶ月分）
{$reg_listimg['file_08']['name']}
";

//お問い合わせメッセージ送信
$body_admin = "
登録日時：$entry_time
ホスト名：$entry_host
ブラウザ：$entry_ua


$email_head_ctm_admin


$msgBody

---------------------------------------------------------------
".$email_body_footer."
---------------------------------------------------------------";

//お客様用メッセージ
$body_user = "
$reg_name 様

$email_head_ctm_user

---------------------------------------------------------------

$msgBody

---------------------------------------------------------------
".$email_body_footer."
---------------------------------------------------------------";


  // ▼ ▼ ▼ START Detect SPAMMER ▼ ▼ ▼ //
  try {
    $allow_send_email = 1;

    // Anti spam advanced version 3 start: Verify by google invisible reCaptcha
    if(empty($_SESSION['ses_from_step2'])) throw new Exception('Step confirm must be display');

    // check spam mail by gtime
    $gtime_step2 = $_GET['g'];
    // submit form dosen't have g
    if(!$gtime_step2) {
      throw new Exception('Miss g request');
    } else {
      $cur_time = time();
      if(strlen($cur_time)!=strlen($gtime_step2)) {
        throw new Exception('G request\'s not a time');
      } elseif( $_SESSION['ses_gtime_step2'] == $gtime_step2 && ($cur_time-$gtime_step2 < 1) ) {
        throw new Exception('Checking confirm too fast');
      }
    }

    // Anti spam advanced version 2 start: Don't send blank emails
    if(empty($reg_name) || empty($reg_email)) {
      throw new Exception('Miss reg_name or reg_email');
    }

    // Anti spam advanced version 1 start: The preg_match() is there to make sure spammers can’t abuse your server by injecting extra fields (such as CC and BCC) into the header.
    if(preg_match( "/[\r\n]/", $reg_email)) {
      throw new Exception('Email\'s not correct');
    }

    // Anti spam: the contact form start
    if($reg_url != "") {
      throw new Exception('Url request must be empty');
    }

    // Anti spam: check session complete contact
    if(!isset($_SESSION['ses_step3'])) $_SESSION['ses_step3'] = false;
    if($_SESSION['ses_step3']) {
      throw new Exception('Session step 3 must be destroy');
    }
  } catch (Exception $e) {
    $returnE = '<pre class="preanhtn">';
    $returnE .= $e->getMessage().'<br>';
    $returnE .= 'File: '.$e->getFile().' at line '.$e->getLine();
    $returnE .= '</pre>';
    $allow_send_email = 0;
    // die($returnE);
  }
  // ▲ ▲ ▲ END Detect SPAMMER ▼ ▼ ▼ //


  if($allow_send_email) {
    //////// メール送信
    mb_language("ja");
    mb_internal_encoding("UTF-8");

    //////// お客様受け取りメール送信
    $email = new JPHPmailer();
    $email->addTo($reg_email);
    $email->setFrom($from,$fromname);
    $email->setSubject($subject_user);
    $email->setBody($body_user);

    if($email->send()) { /*Do you want to debug somthing?*/ }

    //////// メール送信
    $email->clearAddresses();
    for($i = 0; $i < count($aMailto); $i++) $email->addTo($aMailto[$i]);
    for($i = 0; $i < count($aBccTo); $i++) $email->addBcc($aBccTo[$i]);
    $email->setSubject($subject_admin);
    $email->setBody($body_admin);
    if (!empty($reg_listimg)) {
      foreach ($reg_listimg as $item) {
        $email->addAttachment(dirname(__FILE__).'/uploads/'.$item['realname'],$item['name']);
      }
    }

    if($email->Send()) { /*Do you want to debug somthing?*/ }

    // Remove files
    if (!empty($reg_listimg)) {
      foreach ($reg_listimg as $item) {
        unlink(dirname(__FILE__).'/uploads/'.$item['realname']);
      }
    }

    $_SESSION['ses_step3'] = true;
  }

  $_SESSION['statusFlag'] = 1;
  header("Location: ".APP_URL."contact/complete/");
  exit;
}

if(!empty($_SESSION['statusFlag'])) unset($_SESSION['statusFlag']);
else header('location: '.APP_URL);

unset($_SESSION['ses_gtime_step2']);
unset($_SESSION['ses_from_step2']);
unset($_SESSION['ses_step3']);
?>
<meta http-equiv="refresh" content="15; url=<?php echo APP_URL ?>">
<script>
history.pushState({ page: 1 }, "title 1", "#noback");
window.onhashchange = function (event) {
  window.location.hash = "#noback";
};
</script>
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/contact.min.css">
</head>
<body id="contact" class="contact complete_page">
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
  <main>
    <div class="cmn_hd">
      <div class="cmn_hd__inner">
        <div class="cmn_hd__bg">
          <span class="thumb lazy pc" data-bg="<?php echo APP_ASSETS;?>img/contact/img_main.jpg"></span>
          <span class="thumb lazy sp" data-bg="<?php echo APP_ASSETS;?>img/contact/img_main_sp.jpg"></span>
        </div>
        <div class="wcm cmn_hd__title">
          <span class="cmn_hd__title--en">Contact</span>
          <h1 class="cmn_hd__title--jp">お問い合わせ</h1>
        </div>
      </div>
    </div>
    <div class="breadcrumbs">
      <ul class="wcm">
        <li><a href="<?php echo APP_URL;?>">TOP</a></li>
        <li><span>お問い合わせ</span></li>
      </ul>
    </div>
    <div class="wcm contact__inner">
      <div class="complete_page__content">
        <p>メールの送信が完了致しました。<br>担当者より後ほどご返信いたしますので、<br class="sp">しばらくお待ちください。<br>また、確認のためお客様に自動返信メールを<br class="sp">お送りしております。</p>
        <a href="<?php echo APP_URL;?>"><span>トップページに戻る</span></a>
      </div>
    </div>
  </main>
</div>
<?php include(APP_PATH.'libs/footer.php'); ?>
</body>
</html>
