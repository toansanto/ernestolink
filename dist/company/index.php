<?php
$thisPageName = 'company';
include_once(dirname(__DIR__) . '/app_config.php');
include(APP_PATH.'libs/head.php');
?>
<meta http-equiv="expires" content="86400">
<meta name="format-detection" content="telephone=no">
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/company.min.css">
</head>

<body id="company" class="company">
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
    <main>
        <div class="cmn_hd">
            <div class="cmn_hd__inner">
                <div class="cmn_hd__bg">
                    <span
                        class="thumb lazy pc"
                        data-bg="<?php echo APP_ASSETS;?>img/company/img_main.jpg"></span>
                    <span
                        class="thumb lazy sp"
                        data-bg="<?php echo APP_ASSETS;?>img/company/img_main_sp.jpg"></span>
                </div>
                <div class="wcm cmn_hd__title">
                    <span class="cmn_hd__title--en">COMPANY</span>
                    <h1 class="cmn_hd__title--jp">会社紹介</h1>
                </div>
            </div>
        </div>
        <div class="breadcrumbs">
            <ul class="wcm">
                <li>
                    <a href="<?php echo APP_URL;?>">TOP</a>
                </li>
                <li>
                    <span>会社紹介</span></li>
            </ul>
        </div>
        <div class="company__content">
					<div class="content01">
	            <div class="content01__content">
	                <h4>大手企業と同じ立場でコスト交渉を</h4>
	                <p>会社経営で最も大事なこと。それは「キャッシュフロー」です。<br>
	                    キャッシュフローを回すには「経費を下げること」が最も早くて容易です。まずはコスト削減でキャッシュ＝財源を作り、<br>
	                    その財源を使って会社をさらに成長させる。私たちは、その一助になりたいと本気で考えています。<br>
	                    コスト削減はどんな企業でもできます。なぜなら、情報の量がその可否を左右するからです。<br>
	                    私たちには、毎月３０社以上の企業様から削減のご依頼をいただいているため、その都度更新されていく最新のビッグデータがあります。<br>
	                    このデータがあれば、中小企業でも大企業と同じ立場でのコスト交渉が可能です。<br>
	                    また、コスト削減は「しないといけない状況」よりも、黒字が継続できているタイミングで行うほうが大きな成果（利益）を見込めます。<br>
	                    減収減益で経費改善のためにコスト削減を検討するよりも、今すぐコスト削減を検討しませんか？<br>
	                    御社のコスト削減、そして会社の成長の実現をお手伝いさせてください。</p>
	            </div>
	        </div>
	        <div class="content02">
	            <div class="content02__content inner">
	                <div class="content02__content__left">
	                    <img src="<?php echo APP_ASSETS;?>img/company/people.png" alt="谷岡 遼" class="pc">
	                </div>
	                <div class="content02__content__right">
	                    <span>株式会社エルネストリンク</span>
	                    <div class="imgbox01">
	                        <div class="name">
	                            <em>代表取締役</em>
	                            <span>谷岡<em class="pc">&nbsp</em>&nbsp&nbsp遼</span>
	                        </div>
	                        <img
	                            src="<?php echo APP_ASSETS;?>img/company/people-sp.png"
	                            alt="谷岡 遼"
	                            class="sp">
	                    </div>
	                    <p>１９８７年、大阪府松原市生まれ。<br>
	                        大学時代に携帯電話の営業のアルバイトで３年連続上位の成績を残す。その後、２０１１年に携帯電話の販売業で起業。１年後に株式会社エルネストリンクを設立する。設立当初は携帯電話販売の経験を活かして販売を行っていたが、３年目のときに仲の良かった経営者さんが次々と倒産。<br>
	                        中には自ら命を絶った方もいて、大きなショックを受ける。「自分は何の役にも立てていなかった」<br>
	                        無力感に苛まれ、さらに中小企業の７割が赤字で申告している現状を知り、これからは「日本を支える中小企業の発展を支える存在」になることを決意し、コスト削減事業を始める。<br>
	                        これまでの実績として、年商１０億円企業のコストを２０００万円削減。営業利益２００万円の企業を１３倍の２７００万円にアップさせる、などの実績を持つ。趣味は水泳、ボクシング。最近はスノーボード、ウェイクボード、キャンプなどのアウトドア、旅行にハマっている。<br>
	                        人生のミッションは「自分がいたことで救われる人を一人でも増やすこと」。</p>
	                </div>
	            </div>
	        </div>
	        <div class="content03">
	            <ul class="content03__content">
	                <li class="content03__content__box">
	                    <img
	                        src="<?php echo APP_ASSETS;?>img/company/img01.jpg"
	                        alt="森　康二"
	                        class="sp img01">
	                    <div class="content03__content__box__left">
	                        <div class="name">
	                            <span>取締役</span>
	                            <em>森<em class="pc">&nbsp&nbsp</em>&nbsp康二</em>
	                        </div>
	                        <div class="message">MESSAGE</div>
	                        <h5>経営者の方と一緒に未来を考えます</h5>
	                        <p>兵庫県出身<br>
	                            コンサルティング会社にて営業、セミナー、研修講師などの業務に従事。現在は様々な業種業態の中小企業の経営革新や改善の実行支援に取り組んでいる。<br>
	                            2018年よりエルネストリンクに参画。大手ドラッグストア、<br>
	                            IT企業の財務コンサルティングを担当。</p>
	                    </div>
	                    <div class="content03__content__box__right">
	                        <img src="<?php echo APP_ASSETS;?>img/company/img01.jpg" alt="森　康二" class="pc"></div>
	                </li>
	                <li class="content03__content__box box-spec">
	                    <img
	                        src="<?php echo APP_ASSETS;?>img/company/img02.jpg"
	                        alt="相島　恵理"
	                        class="sp img02">
	                    <div class="content03__content__box__right right-spec">
	                        <img src="<?php echo APP_ASSETS;?>img/company/img02.jpg" alt="相島　恵理" class="pc">
	                    </div>
	                    <div class="content03__content__box__left  left-spec">
	                        <div class="name">
	                            <span>統括管理部<em class="pc">&nbsp&nbsp</em>&nbspマネージャー</span>
	                            <em>相島<em class="pc">&nbsp&nbsp</em>&nbsp恵理</em>
	                        </div>
	                        <div class="message">MESSAGE</div>
	                        <h5>ご安心いただけるサポートを提供します</h5>
	                        <p>奈良県出身<br>
	                            株式会社エルネストリンク創業以来、削減シュミレーション・ご提案・導入サポート・経理・管理まで様々な業務を担当。<br class="pc">
	                            これまでの経験を活かし、『迅速』『柔軟』『安心』をモットーに、誠心誠意ご対応させていただきます。</p>
	                    </div>
	                </li>
	                <li class="content03__content__box">
	                    <img
	                        src="<?php echo APP_ASSETS;?>img/company/img03.jpg"
	                        alt="木田　光治"
	                        class="sp img01">
	                    <div class="content03__content__box__left">
	                        <div class="name pc">
	                            <span>コンサルティング営業部&nbsp&nbsp&nbsp<br class="sp">主任</span>
	                            <em>木田&nbsp&nbsp&nbsp光治</em>
	                        </div>
	                        <div class="name sp">
	                            <span>コンサルティング営業部</span><br>
	                            <span class="name-sp">主任&nbsp&nbsp&nbsp<em>木田&nbsp光治</em>
	                            </span>
	                        </div>
	                        <div class="message">MESSAGE</div>
	                        <h5>ご縁をいただいたお客様を笑顔に</h5>
	                        <p>兵庫県淡路島出身<br>
	                            １５年ほど医療・介護関係の仕事に従事。<br class="pc">
	                            コンサルティング営業部としてたくさんのお客様に喜んで頂けるよう真心熱意を持って創意工夫に努める。<br>
	                            どんな些細なことでも気軽にご相談ください。</p>
	                    </div>
	                    <div class="content03__content__box__right">
	                        <img src="<?php echo APP_ASSETS;?>img/company/img03.jpg" alt="木田　光治" class="pc"></div>
	                </li>
	                <li class="content03__content__box box-spec">
	                    <img
	                        src="<?php echo APP_ASSETS;?>img/company/img04.jpg"
	                        alt="相島　恵理"
	                        class="sp img02">
	                    <div class="content03__content__box__right right-spec">
	                        <img src="<?php echo APP_ASSETS;?>img/company/img04.jpg" alt="仲田　真弓" class="pc"></div>
	                    <div class="content03__content__box__left  left-spec">
	                        <div class="name">
	                            <span>ＣＳ事業部&nbsp&nbsp&nbsp<br class="sp">カスタマーサポート</span>
	                            <em>仲田<em class="pc">&nbsp&nbsp</em>&nbsp真弓</em>
	                        </div>
	                        <div class="message">MESSAGE</div>
	                        <h5>つながりを大切に</h5>
	                        <p>大手通信会社での店長業務やバックオフィスを経験。<br class="pc">
	                            現在カスタマーサポート担当として、お客様からの様々な商品・サービスについてのご相談、お問合せの対応をしております。「エルネストリンクに相談して良かった」と言ってもらえるよう、顔が見えない分、言葉遣いには細心の注意を払うとともに、迅速な対応を心掛けております。</p>
	                    </div>
	                </li>
	            </ul>
	        </div>
	        <div class="content04">
	            <div class="content04__performance--title">
	                <p class="title_en">
	                    <span>O</span>UTLINE</p>
	                <h2 class="title_jp">エルネストリンク<br class="sp">会社概要</h2>
	            </div>
	            <ul class="companyinfo inner">
	                <li>
	                    <div class="left">社&nbsp&nbsp&nbsp名</div>
	                    <div class="right">株式会社エルネストリンク</div>
	                </li>
	                <li>
	                    <div class="left">所在地</div>
	                    <div class="right">〒540-0029
	                        <br class="sp">大阪府大阪市中央区本町橋2-17
	                        <br class="sp">AXIS内本町ビル9F</div>
	                </li>
	                <li>
	                    <div class="left">TEL</div>
	                    <div class="right">
	                        <a href="tel:06-6910-1334">06-6910-1334</a>
	                    </div>
	                </li>
	                <li>
	                    <div class="left">代表者</div>
	                    <div class="right">谷岡&nbsp&nbsp&nbsp遼</div>
	                </li>
	                <li>
	                    <div class="left">設&nbsp&nbsp&nbsp立</div>
	                    <div class="right">2013年3月</div>
	                </li>
	                <li>
	                    <div class="left">売&nbsp&nbsp&nbsp上</div>
	                    <div class="right">１億７０００万円</div>
	                </li>
	            </ul>
	            <div class="map inner">
	                <iframe
	                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3280.852757314177!2d135.50929711537935!3d34.68366548043889!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6000e7240e876219%3A0x382439cbb87f4b4e!2z44CSNTQwLTAwMjkg5aSn6Ziq5bqc5aSn6Ziq5biC5Lit5aSu5Yy65pys55S65qmL77yS4oiS77yR77yXIO-8oe-8uO-8qe-8s-WGheacrOeUuuODk-ODqyA5Rg!5e0!3m2!1sja!2sjp!4v1619715351998!5m2!1sja!2sjp"
	                    style="border:0;"
	                    allowfullscreen=""
	                    loading="lazy"></iframe>
	            </div>
	        </div>
        </div>
    </main>
</div>
<?php
include(APP_PATH.'libs/service.php');
include(APP_PATH.'libs/footer.php');
?>
</body>
</html>
