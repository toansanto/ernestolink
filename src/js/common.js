// Slick
function bindSlick(dom, data) {
  var $slider = $(dom);
  if ($slider.hasClass('slick-initialized')) return false;
  $slider.slick(data);
}

function unbindSlick(dom) {
  var $slider = $(dom);
  if (!$slider.hasClass('slick-initialized')) return false;
  $slider.slick('unslick');
}

function handleSlick(dom, data, type) {
  switch (type) {
    case 1:
      if ($(window).width() > 767) {
        bindSlick(dom, data);
      } else {
        unbindSlick(dom);
      }
      break;
    default:
      if ($(window).width() < 768) {
        bindSlick(dom, data);
      } else {
        unbindSlick(dom);
      }
  }
}

// Get header offset
function getHeaderHeight() {
  var h = 0;
  if ($('body').hasClass('no_hd') || $('body').hasClass('no_fixed_hd')) {
    h = 0;
  } else {
    if ($(window).width() < 768) {
      h = 60;
    } else {
      h = 90;
    }
  }
  return h;
}

// Resize thumbnail box height
function thumbImg(dom, ratio_pc, ratio_sp) {
  if (dom && ratio_pc && ratio_sp && $(dom).length > 0) {
    var ratio = ratio_pc;
    if ($(window).width() < 768) {
      ratio = ratio_sp;
    }
    var h = Math.round($(dom).width() / ratio);
    $(dom).css('height', h).addClass('loaded');
  } else {
    return false;
  }
}

// Header animation
function checkScroll() {
  if ($(window).scrollTop() > getHeaderHeight() && $('body').hasClass('no_hd') === false) {
    $('.header,body').addClass('fixed');
  } else {
    $('.header,body').removeClass('fixed');
  }
  if ($(window).scrollTop() > Math.round($(window).height() / 2)) {
    $('.footer__gotop').addClass('show');
  } else {
    $('.footer__gotop').removeClass('show');
  }
}

// Add class for paragraph contains img
function checkParagraph() {
  if ($('.cmsContent').length > 0) {
    $('.cmsContent p').each(function() {
      if ($(this).find('img').length > 0) $(this).addClass('has_img');
    })
  }
}

function IEdetection() {
  var ua = window.navigator.userAgent;
  var msie = ua.indexOf('MSIE ');
  if (msie > 0) {
    // IE 10 or older, return version number
    return ('IE ' + parseInt(ua.substring(
      msie + 5, ua.indexOf('.', msie)), 10));
  }
  var trident = ua.indexOf('Trident/');
  if (trident > 0) {
    // IE 11, return version number
    var rv = ua.indexOf('rv:');
    return ('IE ' + parseInt(ua.substring(
      rv + 3, ua.indexOf('.', rv)), 10));
  }
  var edge = ua.indexOf('Edge/');
  if (edge > 0) {
    //Edge (IE 12+), return version number
    return ('IE ' + parseInt(ua.substring(
      edge + 5, ua.indexOf('.', edge)), 10));
  }
  // User uses other browser
  return ('Not IE');
}
var slider_transform = true;
var result = IEdetection();
if (result !== 'Not IE') {
  $('head').append('<link rel="stylesheet" href="' + app_assets + 'css/page/iefix.min.css">');
  slider_transform = false;
}

// Prevents body scroll on mobile only (including iOS)
$(function () {
  var _overlay = document.getElementById('menu_sp');
  if (_overlay) {
    var _clientY = null;
    _overlay.addEventListener('touchstart', function (event) {
      if (event.targetTouches.length === 1) {
        _clientY = event.targetTouches[0].clientY;
      }
    }, false);
    _overlay.addEventListener('touchmove', function (event) {
      if (event.targetTouches.length === 1) {
        disableRubberBand(event);
      }
    }, false);

    function disableRubberBand(event) {
      var clientY = event.targetTouches[0].clientY - _clientY;
      if (_overlay.scrollTop === 0 && clientY > 0) {
        event.preventDefault();
      }
      if (isOverlayTotallyScrolled() && clientY < 0) {
        event.preventDefault();
      }
    }

    function isOverlayTotallyScrolled() {
      return _overlay.scrollHeight - _overlay.scrollTop <= _overlay.clientHeight;
    }
  }
});

// Load font
if (envInit() === false) {
  WebFontConfig = {
    google: {
      families: ['Noto+Sans+JP:400,700&display=swap&subset=japanese']
    },
    custom: {
      families: ['DIN Alternate','Avenir Roman'],
      urls: [app_assets + 'css/page/fonts.min.css']
    }
  };
  (function () {
    var wf = document.createElement('script');
    wf.src = app_assets + 'js/lib/webfont.min.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
  })();
}

// Add rel 'noopener' and 'noreferrer' for a with _blank target to prevent security rish
$('a')
  .filter('[href^="http"], [href^="//"]')
  .not('[href*="' + window.location.host + '"]')
  .attr('rel', 'noopener')
  .not('.trusted')
  .attr('target', '_blank');

$(window).bind("load resize", function (e) {
  $('a[href^="#"]').on('click', function (e) {
    var widthwin = $(window).width();
    var headerh = getHeaderHeight();
    e.preventDefault();
    var target = this.hash,
      $target = $(target);
    if ($target.length > 0) {
      $('html, body').stop().animate({
        'scrollTop': $target.offset().top - headerh
      }, 500, 'linear', function () {});
    }
  });
});

$(window).bind("load", function (e) {
  var widthwin = $(window).width();
  var headerh = getHeaderHeight();
  var str = location.hash;
  if (str != '' && $(str).length != 0) {
    var n = str.replace("_temp", "");
    $('html,body').stop().animate({
      scrollTop: $(n).offset().top - headerh
    }, 500, 'linear');
  }
});

// Menu SP
$(window).scroll(function () {
  checkScroll();
});
$('#menu_open').click(function () {
  $('body,#header').toggleClass('menu_opened');
});
$('#btn_gotop').click(function () {
  $('html,body').animate({
    scrollTop: 0
  }, 500);
});
$('.header__main--btns li a.btn_service').click(function() {
  $('.header').addClass('dropdown_opened');
  $('.header__main--dropdown').fadeToggle(200);
});
$('.header__main--dropdown .btn_close').click(function() {
  $('.header').removeClass('dropdown_opened');
  $('.header__main--dropdown').fadeOut(200);
});
$('.header__menusp--list>li a.has_sub').click(function() {
  $(this).toggleClass('opened').next('.sub').slideToggle();
})


if ($('.footer__gotop').length > 0) {
  var footer = document.getElementById('copyright');
  window.onscroll = function (ev) {
    var btn_offset = ($(window).width() < 768) ? 28 : 70;
    if (window.pageYOffset >= (footer.offsetTop - footer.clientHeight + btn_offset - window.innerHeight)) {
      $('.footer__gotop').addClass('abs');
    } else {
      $('.footer__gotop').removeClass('abs');
    }
  };
}

$(function () {
  if (location.hash != '' && $(location.hash).length != 0) {
    setTimeout(function() {
      $(window).scrollTop(0);
    },0);
  }
  checkScroll();
  if ($('.lazy').length > 0) {
    var ll = new LazyLoad({
      elements_selector: ".lazy",
      threshold: 0,
    });
  }
  checkParagraph();
});

Pace.on("done", function () {

});
