function check(){
  if(!document.contactform.check1.checked){
    window.alert('「個人情報の取扱いに同意する」にチェックを入れて下さい');
    return false;
  }
}
function scrollForm() {
	if ($('input[name="check1"]').length > 0 && $('input[name="check1"]').is(':checked') === true) {
		offset = getHeaderHeight();
		$('html,body').animate({scrollTop:$('#contactform').offset().top - offset},500);
	}
}
$(function(){
  if ($('#contactform').length > 0) {
    $('#contactform').validationEngine({
      promptPosition: 'topLeft',
      scrollOffset: (getHeaderHeight() + 5),
    });
  }
  $('document').on('click','#btnSend',function(){
    $(this).html('<span>送信中...</span>').attr('disabled', 'disabled').css('opacity', '0.7');
    $('#confirmform').submit();
  });
  // Validate file
	$("input:file").change(function (){
		var span = $(this).parent().find('.note');
		if ($(this).val()) {
			var fileSize = this.files[0].size/1024/1024;
			var val = $(this).val().toLowerCase();
			var regex = new RegExp("(.*?)\.(png|jpg|jpeg|pdf)$");
			if(!(regex.test(val)) || fileSize > 3) {
				$(this).val('');
				alert('添付ファイルは合計で3MB以下になるようにしてください');
			} else {
				var path = $(this).val();
				path = path.split("\\");
				var fileName = path.pop();
				if (fileName != '') {
					span.text(fileName);
				} else {
					span.text('選択されていません');
				}
			}
		} else {
			span.text('選択されていません');
		}
	});
	$("input:file").each(function() {
		var path = $(this).val();
		if(path) {
			path = path.split("\\");
			var fileName = path.pop();
			$(this).parent().find('.note').text(fileName);
		}
	})
})
$(window).load(function() {
  scrollForm();
})
window.onpageshow = function(event) {
  if (event.persisted) {
    scrollForm();
  }
}
