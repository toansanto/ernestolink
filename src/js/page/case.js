function loadAjax(post_type,page,posts_per_page,taxonomy) {
  if (post_type && page && posts_per_page) {
    if (!taxonomy) var taxonomy = '';
    $.ajax({
      type:'POST',
      url:app_url + 'libs/ajax/case.php',
      dataType:'html',
      data:{post_type:post_type,page:page,posts_per_page:posts_per_page,taxonomy:taxonomy},
      beforeSend:function() {
        $('.btn_loadmore').addClass('loading');
      },
      success:function(response) {
        if (response != '') {
          $('.btn_loadmore').attr('data-page',page);
          $('.case_archive__list').append(response);
          if ($('.case_archive__list--item.lastpage').length > 0) {
            $('.btn_loadmore').remove();
          } else {
            $('.btn_loadmore').attr('data-page',parseInt(page)+1);
          };
          $('.btn_loadmore').removeClass('loading');
        }
      },
      complete:function() {
        $('html,body').animate({scrollTop:$('.case_archive__list--item:last').offset().top - getHeaderHeight()},300);
      }
    });
  }
  return false;
}

$(function() {
  $('.btn_loadmore').click(function() {
    var page = $(this).attr('data-page');
    loadAjax('case',page,posts_per_page,taxonomy);
  })
})
